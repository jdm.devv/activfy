﻿using UnityEditor;
using UnityEngine;
using UnityEditor.PackageManager;
using UnityEditor.Callbacks;

namespace Activfy.Editor
{
    [InitializeOnLoad]
    public class ActivfiEditorHelp
    {
        static ActivfiEditorHelp()
        {
            EditorApplication.delayCall += CheckPackageInstallation;
        }
        
        private static void CheckPackageInstallation()
        {
            if (SessionState.GetBool("HideActivfiInfoDisplay", false)) return;
            ActivfiStartup.ShowInfoWindow();
            SessionState.SetBool("HideActivfiInfoDisplay", true);
        }
    }
}