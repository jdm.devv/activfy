using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Activfy.Editor
{
    public class ActivfiStartup : EditorWindow
    {
        [MenuItem("Window/UI Toolkit/ActivfiStartup")]
        public static void ShowInfoWindow()
        {
            ActivfiStartup wnd = GetWindow<ActivfiStartup>();
            wnd.titleContent = new GUIContent("Activfi");
        }

        public void CreateGUI()
        {
            // Each editor window contains a root VisualElement object
            VisualElement root = rootVisualElement;

            // Import UXML
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Activfy/Editor/ActivfiStartup.uxml");
            VisualElement labelFromUXML = visualTree.Instantiate();
            root.Add(labelFromUXML);

            // A stylesheet can be added to a VisualElement.
            // The style will be applied to the VisualElement and all of its children.
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Activfy/Editor/ActivfiStartup.uss");
        }
    }
}