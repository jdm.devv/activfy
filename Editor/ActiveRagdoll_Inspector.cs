﻿using System.IO;
using Activfy.Core;
using UnityEngine;

namespace Activfy.Editor
{
    using UnityEditor;
    using UnityEditor.UIElements;
    using UnityEngine.UIElements;
    
    [CustomEditor(typeof(ActiveRagdoll))]
    public class ActiveRagdollInspector : Editor
    {
        private static bool showAdvanced = false;
        
        public override VisualElement CreateInspectorGUI()
        {
            // Create a new VisualElement to be the root of our inspector UI
            VisualElement myInspector = new VisualElement();

            var csScriptPath = AssetDatabase.GUIDToAssetPath("773881d391aafea4bb6db134da17f4f8");
            var csFileName = Path.GetFileNameWithoutExtension(csScriptPath);
            var csDirectory = Path.GetDirectoryName(csScriptPath);
 
            // Import UXML
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>($"{csDirectory}/{csFileName}.uxml");

            //VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Activfy/Editor/ActiveRagdollUXML.uxml");
            visualTree.CloneTree(myInspector);

            VisualElement configAdvanced = myInspector.Q("ConfigAdvanced");
            SetElementVisiblity(showAdvanced, configAdvanced);
            
            Toggle showAdvancedToggle = myInspector.Q<Toggle>("ShowAdvancedToggle");
            showAdvancedToggle.value = showAdvanced;
            showAdvancedToggle.RegisterValueChangedCallback(evt => OnShowAdvancedChange(showAdvancedToggle.value, configAdvanced));

            ActiveRagdoll activeRagdoll = (ActiveRagdoll)target;

            bool isSetupAlready = activeRagdoll.AllJointData.Count > 0;
            
            UnityEngine.UIElements.Button documentationButton = myInspector.Q<UnityEngine.UIElements.Button>("DocumentationButton");
            documentationButton.clicked += () => Application.OpenURL("https://activfi.com/");
            
            UnityEngine.UIElements.Button remassButton = myInspector.Q<UnityEngine.UIElements.Button>("RemassButton");
            UnityEngine.UIElements.Button setupButton = myInspector.Q<UnityEngine.UIElements.Button>("SetupButton");
            UnityEngine.UIElements.Button resetButton = myInspector.Q<UnityEngine.UIElements.Button>("ResetButton");
            
            UnityEngine.UIElements.Button nestJointsButton = myInspector.Q<UnityEngine.UIElements.Button>("NestJointsButton");
            UnityEngine.UIElements.Button unNestJointsButton = myInspector.Q<UnityEngine.UIElements.Button>("UnNestJointsButton");
            
            UnityEngine.UIElements.Button jointPresetsButton = myInspector.Q<UnityEngine.UIElements.Button>("JointPresetsButton");
            UnityEngine.UIElements.Button updateJointSpaceButton = myInspector.Q<UnityEngine.UIElements.Button>("UpdateJointSpace");

            updateJointSpaceButton.clicked += () => activeRagdoll.UpdateJointSpace();
            
            remassButton.clicked += () => activeRagdoll.Remass();
            jointPresetsButton.clicked += () => ActiveRagdollPresets.ShowWindow(activeRagdoll);
            // nestJointsButton.clicked += () => activeRagdoll.NestJoints();
            // unNestJointsButton.clicked += () => activeRagdoll.UnNestJoints();
            
            setupButton.clicked += () =>
            {
                activeRagdoll.Setup();
                
                SetElementVisiblity(false, setupButton);
                SetElementVisiblity(true, remassButton);
                SetElementVisiblity(true, resetButton);
            };
            
            resetButton.clicked += () =>
            {
                if (!EditorUtility.DisplayDialog("Reset joint configuration", "Are you sure you want to reset the configuration of the joints?", "Yes, reset it", "No, don't reset it"))
                    return;
                
                activeRagdoll.ResetActiveRagdoll();
                    
                SetElementVisiblity(true, setupButton);
                SetElementVisiblity(false, remassButton);
                SetElementVisiblity(false, resetButton);
            };

            SetElementVisiblity(!isSetupAlready, setupButton);
            SetElementVisiblity(isSetupAlready, remassButton);
            SetElementVisiblity(isSetupAlready, resetButton);

            return myInspector;
        }

        private void OnShowAdvancedChange(bool value, VisualElement element)
        {
            showAdvanced = value;
            SetElementVisiblity(showAdvanced, element);
        }

        private static void SetElementVisiblity(bool value, VisualElement element)
        {
            element.style.display = value ? DisplayStyle.Flex : DisplayStyle.None;
        }
    }
}