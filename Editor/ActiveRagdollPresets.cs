using Activfy.Core;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;


public class ActiveRagdollPresets : EditorWindow
{
    public ActiveRagdoll EditingActiveRagdoll;
    
    public static void ShowWindow(ActiveRagdoll editingRagdoll)
    {
        ActiveRagdollPresets wnd = GetWindow<ActiveRagdollPresets>();
        wnd.SetActiveRagdoll(editingRagdoll);
        wnd.titleContent = new GUIContent("Active Ragdoll Presets");
    }

    public void CreateGUI()
    {
        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        // Import UXML
        var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Activfy/Editor/ActiveRagdollPresets.uxml");
        VisualElement labelFromUXML = visualTree.Instantiate();
        root.Add(labelFromUXML);
    }

    public void SetActiveRagdoll(ActiveRagdoll editingRagdoll)
    {
        EditingActiveRagdoll = editingRagdoll;
        
        ObjectField editingActiveRagdollField = rootVisualElement.Q<ObjectField>("EditingActiveRagdoll");
        editingActiveRagdollField.value = EditingActiveRagdoll;
        
        Button goofySetButton = rootVisualElement.Q<Button>("GoofyButton");
        Button rigidSetButton = rootVisualElement.Q<Button>("RigidButton");
        
        goofySetButton.clicked += () => SetJointStrengthByMass(.2f);
        rigidSetButton.clicked += () => SetJointStrengthByMass(.8f);
    }

    private void SetJointStrengthByMass(float percent)
    {
        for (int i = 1; i < EditingActiveRagdoll.AllJointData.Count; i++)
        {
            JointData jointData = EditingActiveRagdoll.AllJointData[i];
            var mass = jointData.Rigid.mass;
            jointData.Spring = mass * percent * 2000;
            jointData.Damper = mass * percent * 30;
        }
    }
}