﻿using System;
using Activfy.Additions;
using Activfy.Helpers;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace Activfy.Core
{
    [Serializable]
    public class JointData : IEquatable<JointData>
    {
        [ReadOnly, SerializeField] private string Name;
        [ReadOnly] public ConfigurableJoint Joint;
        [ReadOnly] public Rigidbody Rigid;
        [ReadOnly] public Transform AnimatedBone;
        [HideInInspector] public JointData Parent;
        public bool ShouldSync = true;
        public bool ShowDebug = false;

        [Header("Static Elements")] public float JointLerpSpeed;
        public float Spring;
        public float Damper;
        /// <summary>
        /// Multiplier to Spring and Damper depending on the joint's animated movement
        /// </summary>
        public float MovementMultiplier = 1.0f;
        [ReadOnly] public float MuscularPower;

        [ReadOnly] public Quaternion InitialQuat;
        [ReadOnly] private ActiveRagdoll _connectedActiveRagdoll;
        
        // Runtime Data
        private AnimatedBoneSnap _previousSnap;

        public JointData(ActiveRagdoll activeRagdoll,
            ConfigurableJoint joint,
            Quaternion initialQuat,
            Transform animatedBone,
            float jointLerpSpeed = 7.0f)
        {
            _connectedActiveRagdoll = activeRagdoll;
            Name = joint.gameObject.name;
            Joint = joint;
            JointLerpSpeed = jointLerpSpeed;
            InitialQuat = initialQuat;
            AnimatedBone = animatedBone;
            Spring = 4500;
            Damper = 40;
            Rigid = joint.GetComponent<Rigidbody>();
        }

        public void Awake()
        {
            
        }

        public void FixedUpdate()
        {
            AnimatedBoneSnap currentSnap = new AnimatedBoneSnap
            {
                LocalRotation = AnimatedBone.localRotation,
                Position = AnimatedBone.position
            };
            Vector3 targetAngularVelocity = CalculateAngularVelocity(_previousSnap, currentSnap, Time.fixedDeltaTime);
            SetTargetAngularVelocityLocal(targetAngularVelocity);
            
            DynamicMuscularJoints(currentSnap);
            
            _previousSnap = currentSnap;
        }

        private void DynamicMuscularJoints(AnimatedBoneSnap currentSnap)
        {
            Vector3 velocity = CalculateVelocity(_previousSnap, currentSnap, Time.fixedDeltaTime);
            float magnitude = velocity.magnitude;
            float muscularPower = MovementMultiplier * magnitude;

            MuscularPower = muscularPower;
            
            UpdateSpringAndDamper(Spring + (Spring * muscularPower), Damper + (Damper * muscularPower));
        }
        
        /// <summary>
        /// At OnEnable(), joints do some weird re-configuring. This method, if called from OnEnable(), deals with that.
        /// </summary>
        internal void ResetJointAxisOnEnable(ActiveRagdoll activeRagdoll)
        {
            // https://forum.unity.com/threads/hinge-joint-limits-resets-on-activate-object.483481/#post-5713138
            var jointTransform = Rigid.transform;
            if (activeRagdoll.InWorldSpace)
            {
                Quaternion originalRotation = jointTransform.rotation;
                jointTransform.rotation = InitialQuat;
                Joint.axis = Joint.axis; // Yes, this is intentional. The axis setter triggers some calculations that we need.
                jointTransform.rotation = originalRotation;
            }
            else
            {
                Quaternion originalRotation = jointTransform.localRotation;
                jointTransform.localRotation = InitialQuat;
                Joint.axis = Joint.axis; // Yes, this is intentional. The axis setter triggers some calculations that we need.
                jointTransform.localRotation = originalRotation;
            }
        }
        
        void SetTargetAngularVelocityLocal(Vector3 targetAngularVelocity)
        {
            Joint.targetAngularVelocity = InitialQuat * targetAngularVelocity;
        }
        
        static Vector3 CalculateAngularVelocity(AnimatedBoneSnap previousSnap, AnimatedBoneSnap newSnap, float deltaTime)
        {
            Quaternion deltaRotation = newSnap.LocalRotation * Quaternion.Inverse(previousSnap.LocalRotation);
            deltaRotation.ToAngleAxis(out float deltaAngle, out Vector3 axis);

            if (deltaAngle > 180)
            {
                deltaAngle -= 360f;
            }

            return Mathf.Deg2Rad * deltaAngle / deltaTime * axis.normalized;
        }
        
        static Vector3 CalculateVelocity(AnimatedBoneSnap previousSnap, AnimatedBoneSnap newSnap, float deltaTime)
        {
            // Calculate the change in position (displacement)
            Vector3 deltaPosition = newSnap.Position - previousSnap.Position;

            // Calculate velocity as displacement over time
            Vector3 velocity = deltaPosition / deltaTime;

            return velocity;
        }

        public void UpdateSpringAndDamper()
        {
            if (!Joint) return;

            JointDrive jointSlerpDrive = Joint.slerpDrive;
            jointSlerpDrive.positionSpring = Spring;
            jointSlerpDrive.positionDamper = Damper;
            Joint.slerpDrive = jointSlerpDrive;
        }
    
        public void UpdateSpringAndDamper(float spring, float damper)
        {
            if (!Joint) return;

            JointDrive jointSlerpDrive = Joint.slerpDrive;
            jointSlerpDrive.positionSpring = spring;
            jointSlerpDrive.positionDamper = damper;
            Joint.slerpDrive = jointSlerpDrive;
        }

        public bool Equals(JointData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Name == other.Name &&
                   Equals(Joint, other.Joint) &&
                   Equals(Rigid, other.Rigid) &&
                   JointLerpSpeed.Equals(other.JointLerpSpeed) &&
                   Spring.Equals(other.Spring) &&
                   Damper.Equals(other.Damper) &&
                   InitialQuat.Equals(other.InitialQuat);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((JointData) obj);
        }

        public override int GetHashCode()
        {
            // ReSharper disable NonReadonlyMemberInGetHashCode
            return HashCode.Combine(Name, Joint, Rigid, JointLerpSpeed, Spring, Damper, InitialQuat);
        }

        public void GoLimp()
        {
            UpdateSpringAndDamper(0, 0);
        }
        
        public void GoFirm()
        {
            UpdateSpringAndDamper();
        }

        struct AnimatedBoneSnap
        {
            internal Quaternion LocalRotation;
            internal Vector3 Position;
        }
    }
}