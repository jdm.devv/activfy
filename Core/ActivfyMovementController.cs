using System;
using Activfy.Additions;
using Activfy.Helpers;
using UnityEngine;

namespace Activfy.Core
{
    [AddComponentMenu("Activfy/Core/Movement Controller")]
    [RequireComponent(typeof(ActiveRagdoll))]
    public class ActivfyMovementController : MonoBehaviour
    {
        public Vector3 MoveDirection;
        public float MaxMoveSpeed = 3;
        public float Acceleration = 20;
        public float MovementModifer = 1;
        public float sidewaysFriction = 1;
        public JointData RagdollHips => ConnectedActiveRagdoll.RagdollPelvis;

        protected ActiveRagdoll ConnectedActiveRagdoll;
        
        protected ActivfyStabilityController StabilityController;
        private ActivfyDamageSystem _damageSystem;

        private void Awake()
        {
            ConnectedActiveRagdoll = GetComponent<ActiveRagdoll>();
            StabilityController = GetComponent<ActivfyStabilityController>();
            _damageSystem = GetComponent<ActivfyDamageSystem>();
        }

        protected virtual void FixedUpdate()
        {
            if (StabilityController && StabilityController.IsDown) return;
            
            MovementUpdate();
        }

        protected void MovementUpdate()
        {
            float maxSpeed = MaxMoveSpeed;
            float acceleration = Acceleration * MovementModifer;

            if (_damageSystem && _damageSystem.enabled)
            {
                maxSpeed *= _damageSystem.TotalMovementModify;
                acceleration *= _damageSystem.TotalMovementModify;
            }
            
            Vector3 steeringDir = ConnectedActiveRagdoll.RagdollPelvis.Rigid.transform.right; // _connectedActiveRagdoll.GetLocalPelvisDirection(_connectedActiveRagdoll.PelvisRight)
            Vector3 velocity = ConnectedActiveRagdoll.RagdollPelvis.Rigid.velocity;

            // Calculate the amount of friction based on the direction of movement
            float steeringVel = Vector3.Dot(steeringDir, velocity);

            Vector3 desiredVelChange = steeringDir * (-steeringVel * sidewaysFriction);
            Vector3 accelerationForce = MoveDirection * (acceleration);

            foreach (JointData joint in ConnectedActiveRagdoll.AllJointData)
            {
                Rigidbody jointRigidbody = joint.Rigid;

                ActivfiUtil.AddForceSafe(ref jointRigidbody, (accelerationForce + desiredVelChange) * Time.fixedDeltaTime, maxSpeed);
            }
        }
        
        public void SetMoveDirection(Vector3 direction)
        {
            MoveDirection = direction;
        }
    }
}
