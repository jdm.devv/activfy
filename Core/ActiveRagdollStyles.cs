﻿namespace Activfy.Core
{
    public enum ActiveRagdollStyles
    {
        Dynamic,
        Fixed,
        Hybrid
    }
}