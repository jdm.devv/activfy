﻿using System;
using UnityEngine;

namespace Activfy.Core
{
    [AddComponentMenu("Activfy/Core/Animation Controller")]
    [RequireComponent(typeof(ActiveRagdoll), typeof(ActivfyMovementController))]
    public class ActivfyAnimationController : MonoBehaviour
    {
        [SerializeField] private Animator _connectedAnimator;
        private ActivfyMovementController _movementController;
        
        private void Awake()
        {
            _movementController = GetComponent<ActivfyMovementController>();
        }

        private void Update()
        {
            // get the velocity vector of the Rigidbody
            Vector3 velocity = _movementController.RagdollHips.Rigid.velocity;

            // calculate the forward speed by projecting the velocity vector onto the forward direction of the GameObject
            float forwardSpeed = Vector3.Dot(velocity, _movementController.RagdollHips.Rigid.transform.forward);
            
            _connectedAnimator.SetFloat("andyMovement", forwardSpeed);
        }
    }
}