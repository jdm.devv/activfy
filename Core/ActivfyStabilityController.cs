﻿using System;
using Activfy.Additions;
using Activfy.Helpers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Activfy.Core
{
    [AddComponentMenu("Activfy/Core/Stability Controller")]
    public class ActivfyStabilityController : MonoBehaviour
    {
        [SerializeField] private float _walkingHeightOffset = 0;
        [SerializeField] private LayerMask _heightCastMask;
        [SerializeField] private float _rideSpringStrength = 6500;
        [SerializeField] private float _rideSpringDamper = 200;
        [SerializeField] private float _uprightSpringStrength = 500;
        [SerializeField] private float _headStabilityStrength = 10000;
        [SerializeField] private float _uprightSpringDamper = 10;
        [FormerlySerializedAs("_stablityPercentWhenAirborne")] [SerializeField] private float _stabilityPercentWhenAirborne = 0.5f;
        [SerializeField] private float _coreDampingCoefficient = 0.1f;  

        [Header("Falling")] public bool CanFall;
        [ReadOnly, SerializeField] private float _getUpTimeLeft;
        private float _fallImmuneTimeLeft;
        private bool _isFallImmuneTillGrounded;
        public Rigidbody RagdollHead;
        public Transform AnimatedHead;
        [SerializeField] private float _hipsFallAngle = 40;
        [SerializeField] private float _headFallAngle = 50;
        [SerializeField] private bool _isDown;
        [SerializeField] private float _getUpTime = 3;

        [SerializeField] private bool _applyForceToSteppedOnRigidbodies = false;
        [SerializeField] private bool _useAdvancedFootInstability = false;
        [SerializeField] private Rigidbody _leftFoot;
        [SerializeField] private Rigidbody _rightFoot;
        
        private float _airborneScaleThreshold = 1.5f;
        private float _maxRagdollDistance = 0f;
        
        [HideInInspector] public UnityEvent OnGetUp = new UnityEvent();
        [HideInInspector] public UnityEvent OnGetUpFront = new UnityEvent();
        [HideInInspector] public UnityEvent OnGetUpBack = new UnityEvent();

        [Header("Current Stats"), ReadOnly, SerializeField]
        private float _currentDistanceFromGround;

        [SerializeField, ReadOnly] private float _currentHipsAngle;

        private float _walkingHeight = 1;

        public bool IsDown => _isDown;

        public JointData RagdollHips => _connectedActiveRagdoll.RagdollPelvis;
        public Transform AnimatedHips => _connectedActiveRagdoll.AnimatedPelvis;

        private ActiveRagdoll _connectedActiveRagdoll;
        private ActivfyDamageSystem _damageSystem;

        private void Awake()
        {
            _connectedActiveRagdoll = GetComponent<ActiveRagdoll>();
            _damageSystem = GetComponent<ActivfyDamageSystem>();

            //_upForcePID = new PID(1, 1, 1);
        }

        private void OnDisable()
        {
            RagdollHips.GoLimp();
        }

        private void OnEnable()
        {
            RagdollHips.GoFirm();
        }

        private void FixedUpdate()
        {
            StabilityUpdate();
        }

        private void StabilityUpdate()
        {
            FallUpdate();
            BalanceUpdate();
        }

        private void Start()
        {
            CalculateRagdollHeight();
        }

        private void BalanceUpdate()
        {
            if (_useAdvancedFootInstability && IsUnstable() && !_isDown && _fallImmuneTimeLeft <= 0 && !_isFallImmuneTillGrounded)
            {
                Fall();
            }

            if (_isDown)
                return;

            SpringForceUpUpdate();
            UprightForceUpdate();
            CoreStabilityUpdate();
            HeadStabilityUpdate();
        }

        private void SpringForceUpUpdate()
        {
            // Calculate walking height
            CalculateSpringHeight();

            Vector3 rayDir = Vector3.down;
            const float maxDistance = 1000f;

            Ray ray = new Ray(RagdollHips.Rigid.position, rayDir);
            const float castRadius = 0.3f;

            if (!Physics.SphereCast(ray, castRadius, out RaycastHit hit, maxDistance, _heightCastMask))
                return;

            // remove later
            Debug.DrawLine(ray.origin, hit.point);

            //Debug.Log(hit.distance + " " + hit.collider.gameObject.name);

            Vector3 vel = RagdollHips.Rigid.velocity;
            Vector3 otherVel = Vector3.zero;

            if (hit.rigidbody)
            {
                otherVel = hit.rigidbody.velocity;
            }

            float rayDirVel = Vector3.Dot(rayDir, vel);
            float otherDirVel = Vector3.Dot(rayDir, otherVel);
            float relVel = rayDirVel - otherDirVel;

            float error = hit.distance - _walkingHeight;

            float springForce = -((error * _rideSpringStrength) - (relVel * _rideSpringDamper));

            _currentDistanceFromGround = hit.distance;

            if (springForce <= 0)
                return;

            RagdollHips.Rigid.AddForce(-rayDir * springForce);

            if (_applyForceToSteppedOnRigidbodies && hit.rigidbody)
            {
                hit.rigidbody.AddForceAtPosition(rayDir * springForce, hit.point);
            }
        }
        
        private void UprightForceUpdate()
        {
            Quaternion targetUpright = AnimatedHips.rotation;
            Quaternion characterCurrent = RagdollHips.Rigid.rotation;
            Quaternion toGoal = ActivfiUtil.ShortestRotation(targetUpright, characterCurrent);

            toGoal.ToAngleAxis(out float rotDegrees, out Vector3 rotAxis);
            rotAxis.Normalize();

            float rotRadians = rotDegrees * Mathf.Deg2Rad;

            // Calculate spring force
            Vector3 springTorque = rotAxis * (rotRadians * _uprightSpringStrength);

            // Calculate damper force
            Vector3 damperTorque = -RagdollHips.Rigid.angularVelocity * _uprightSpringDamper;

            float modifier = 1.0f;
            if (IsAirborne())
                modifier = _stabilityPercentWhenAirborne;

            // Split torque into Y-axis (left/right rotation) and other axes (X and Z)
            Vector3 springTorque_Y = Vector3.Project(springTorque, Vector3.up);
            Vector3 springTorque_XZ = springTorque - springTorque_Y;

            Vector3 damperTorque_Y = Vector3.Project(damperTorque, Vector3.up);
            Vector3 damperTorque_XZ = damperTorque - damperTorque_Y;

            // Apply torque for Y-axis (left/right rotation)
            RagdollHips.Rigid.AddTorque((springTorque_Y + damperTorque_Y) * (modifier * Time.fixedDeltaTime * 30), ForceMode.Acceleration);

            // Apply torque for XZ axes (tilt corrections)
            RagdollHips.Rigid.AddTorque((springTorque_XZ + damperTorque_XZ) * (modifier * Time.fixedDeltaTime * 170), ForceMode.Acceleration);
        }

        // private void UprightForceUpdate()
        // {
        //     Quaternion targetUpright = AnimatedHips.rotation;
        //     Quaternion characterCurrent = RagdollHips.Rigid.rotation;
        //     Quaternion toGoal = ActivfiUtil.ShortestRotation(targetUpright, characterCurrent);
        //
        //     toGoal.ToAngleAxis(out float rotDegrees, out Vector3 rotAxis);
        //     rotAxis.Normalize();
        //
        //     //var hipsPosition = RagdollHips.Rigid.transform.position;
        //     //Debug.DrawRay(hipsPosition, AnimatedHips.forward, Color.green);
        //     //Debug.DrawRay(hipsPosition, RagdollHips.Rigid.transform.forward, Color.yellow);
        //
        //     float rotRadians = rotDegrees * Mathf.Deg2Rad;
        //
        //     // Calculate spring force
        //     Vector3 springTorque = rotAxis * (rotRadians * _uprightSpringStrength);
        //
        //     // Calculate damper force
        //     Vector3 damperTorque = -RagdollHips.Rigid.angularVelocity * _uprightSpringDamper;
        //
        //     float modifier = 1.0f;
        //     if (IsAirborne())
        //         modifier = _stabilityPercentWhenAirborne;
        //
        //     // Apply the total torque to the rigidbody
        //     RagdollHips.Rigid.AddTorque((springTorque + damperTorque) * (modifier * Time.fixedDeltaTime * 170),
        //         ForceMode.Acceleration);
        //
        //     // if (!IsAirborne())
        //     // {
        //     //     // Stability correction
        //     //     float step = 50 * Time.fixedDeltaTime;
        //     //
        //     //     // Rotate our transform a step closer to the target's.
        //     //     Quaternion stepRotation = Quaternion.RotateTowards(characterCurrent, targetUpright, step);
        //     //
        //     //     RagdollHips.Rigid.MoveRotation(stepRotation);
        //     // }
        // }

        private void CalculateSpringHeight()
        {
            Transform pelvisTransform = AnimatedHips;
            Transform rootTransform = pelvisTransform.parent;
            
            float springHeight = pelvisTransform.position.y - rootTransform.position.y;

            _walkingHeight = springHeight + _walkingHeightOffset;
        }

        private void CoreStabilityUpdate()
        {
            // Calculate the damping torque
            var angularVelocity = RagdollHips.Rigid.angularVelocity;

            // Reduce the angular velocity directly
            angularVelocity *= _coreDampingCoefficient;
            RagdollHips.Rigid.angularVelocity = angularVelocity;
        }
        
        private void HeadStabilityUpdate()
        {
            Vector3 ragdollLocalHeadToPelvis = RagdollHips.Rigid.position - RagdollHead.position;
            Vector3 animatedLocalHeadToPelvis = AnimatedHips.position - AnimatedHead.position;

            Vector3 differenceFromAnimated = ragdollLocalHeadToPelvis - animatedLocalHeadToPelvis;
            Debug.DrawRay(RagdollHead.position + Vector3.up, differenceFromAnimated, Color.red);
            Debug.DrawRay(RagdollHead.position + Vector3.up, Vector3.up, Color.cyan);
            RagdollHead.AddForce(differenceFromAnimated * _headStabilityStrength, ForceMode.Acceleration);
            
            // Stability correction
            //float step = 500 * Time.fixedDeltaTime;

            // Rotate our transform a step closer to the target's.
            //Vector3 stepPos = Vector3.MoveTowards(RagdollHead.position, RagdollHead.position + differenceFromAnimated, step);

            //RagdollHead.MovePosition(stepPos);
        }

        private void FallUpdate()
        {
            if (_fallImmuneTimeLeft > 0)
            {
                _fallImmuneTimeLeft -= Time.fixedDeltaTime;
            } 
            else if (_isFallImmuneTillGrounded && !IsAirborne())
            {
                _isFallImmuneTillGrounded = false;
            }
            
            if (!_isDown)
            {
                if (!CanFall) 
                    return;

                if (_fallImmuneTimeLeft > 0)
                    return;

                if (_isFallImmuneTillGrounded)
                    return;

                float hipsAngle =
                    Vector3.Angle(_connectedActiveRagdoll.GetLocalPelvisDirection(_connectedActiveRagdoll.PelvisUp),
                        Vector3.up);
                float headAngle =
                    Vector3.Angle(
                        _connectedActiveRagdoll.GetLocalJointDirection(_connectedActiveRagdoll.PelvisUp, RagdollHead.transform),
                        Vector3.up);

                _currentHipsAngle = hipsAngle;

                if (hipsAngle > _hipsFallAngle || headAngle > _headFallAngle)
                {
                    Fall();
                }
            }
            else
            {
                // Reset get up timer if they aren't stable
                if (_useAdvancedFootInstability && IsUnstable())
                {
                    _getUpTimeLeft = _getUpTime;
                }
                
                _getUpTimeLeft -= Time.fixedDeltaTime;
                if (_getUpTimeLeft < 0)
                {
                    GetUp();
                }
            }
        }

        public void Fall()
        {
            _isDown = true;
            _getUpTimeLeft = _getUpTime;

            _connectedActiveRagdoll.GoLimp();
        }

        public void GetUp()
        {
            InvokeGetUpEvents();

            Animator animator = _connectedActiveRagdoll.GetAnimator();

            // Get the AnimatorStateInfo for the current animation in the specified layer
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

            // Calculate the current time left in seconds
            float clipLength = stateInfo.length; // Length of the current clip in seconds
            float currentTime = stateInfo.normalizedTime * clipLength; // Current time in seconds
            float timeLeft = currentTime - clipLength; // Time left in the current animation

            Debug.LogWarning($"{timeLeft} time left in animation");

            SetFallImmuneTime(Mathf.Max(1.5f, timeLeft));

            _isDown = false;

            _connectedActiveRagdoll.GoFirm();
        }

        public void InvokeGetUpEvents()
        {
            OnGetUp?.Invoke();
            
            // Compute the dot product between pelvis up vector and world up vector
            float dotProduct = Vector3.Dot(Vector3.up, 
                                           _connectedActiveRagdoll.GetLocalPelvisDirection(_connectedActiveRagdoll.PelvisForward));

            if (dotProduct < 1f) // Character is on their back.
            {
                OnGetUpBack?.Invoke();
            } 
            else // Character is on their front.
            {
                OnGetUpFront?.Invoke();
            }
        }

        public void SetFallImmuneTime(float fallImmuneTime)
        {
            _fallImmuneTimeLeft = fallImmuneTime;
        }
        
        public float GetFallImmuneTime()
        {
            return _fallImmuneTimeLeft;
        }

        public void SetIsFallImmuneTillGrounded(bool isFallImmuneTillGrounded)
        {
            _fallImmuneTimeLeft = 1.0f;
            _isFallImmuneTillGrounded = isFallImmuneTillGrounded;
        }

        public bool IsAirborne()
        {
            return _currentDistanceFromGround > GetAirborneHeightThreshold();
        }

        private float GetAirborneHeightThreshold()
        {
            return Mathf.Max(_walkingHeight * _airborneScaleThreshold, _maxRagdollDistance / 2f);
        }
        
        private bool IsUnstable()
        {
            if (IsFootUnstable(_leftFoot) || IsFootUnstable(_rightFoot))
                return true;
            
            return false;
        }

        private bool IsFootUnstable(Rigidbody footRigidBody)
        {
            Vector3 rayDir = Vector3.down;
            const float maxDistance = 1000f;

            Ray ray = new Ray(footRigidBody.position, rayDir);
            
            if (!Physics.Raycast(ray, out RaycastHit hit, maxDistance, _heightCastMask))
                return true;
            
            return hit.distance > GetAirborneHeightThreshold();
        }

        public void CalculateRagdollHeight()
        {
            _maxRagdollDistance = 0f; // Reset distance before recalculating

            // Iterate over all joint pairs
            for (int i = 0; i < _connectedActiveRagdoll.AllJointData.Count; i++)
            {
                for (int j = i + 1; j < _connectedActiveRagdoll.AllJointData.Count; j++)
                {
                    // Get the position of the two joints
                    Vector3 jointPos1 = _connectedActiveRagdoll.AllJointData[i].Rigid.transform.position;
                    Vector3 jointPos2 = _connectedActiveRagdoll.AllJointData[j].Rigid.transform.position;

                    // Calculate the distance between these two joints
                    float distance = Vector3.Distance(jointPos1, jointPos2);

                    // Update _maxRagdollDistance if this distance is the greatest found so far
                    if (distance > _maxRagdollDistance)
                    {
                        _maxRagdollDistance = distance;
                    }
                }
            }

            Debug.Log("Max Ragdoll Distance: " + _maxRagdollDistance);
        }
    }
}