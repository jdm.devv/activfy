using System;
using System.Collections.Generic;
using System.Numerics;
using Activfy.Additions;
using activfy.EditorWizards;
using Activfy.Helpers;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace Activfy.Core
{
    [AddComponentMenu("Activfy/Generated/Create Active Ragdoll...")]
    public class ActiveRagdoll : MonoBehaviour
    {
        public ActiveRagdollStyles Style;
        public Transform AnimatedRoot;
        public Transform RagdollRoot;
        public VectorDirections PelvisUp;
        public VectorDirections PelvisForward;
        public JointData RagdollPelvis => AllJointData[0];
        public Transform AnimatedPelvis;
        public bool ShouldSync = true;
        public bool InWorldSpace = true;
        public bool TestOffset = true;
        [SerializeReference] public List<JointData> AllJointData;
        [SerializeField] private List<Transform> _animatedParts;
        [SerializeField] private List<Transform> _ragdollParts;
        private Dictionary<Transform, JointData> _jointDataDictionary = new Dictionary<Transform, JointData>();

        private ActivfyMovementController _movementController;
        private ActivfyStabilityController _stabilityController;

        public VisualTreeAsset visualTree;

        [HideInInspector] public ActivfyDamageSystem DamageSystem;

        void Awake()
        {
            _movementController = GetComponent<ActivfyMovementController>();
            _stabilityController = GetComponent<ActivfyStabilityController>();
            DamageSystem = GetComponent<ActivfyDamageSystem>();

            for (int i = 0; i < AllJointData.Count; i++)
            {
                JointData jointData = AllJointData[i];

                //jointData.InitialQuat = InWorldSpace ? jointData.Joint.transform.rotation : jointData.Joint.transform.localRotation;

                if (jointData.Joint)
                {
                    _jointDataDictionary.Add(jointData.Joint.transform, jointData);
                }

                jointData.Awake();
            }

            for (int i = 0; i < AllJointData.Count; i++)
            {
                JointData jointData = AllJointData[i];
                
                if (!jointData.Joint.connectedBody)
                    continue;

                jointData.Parent = _jointDataDictionary[jointData.Joint.connectedBody.transform];
            }

            UpdateSpringAndDamper();

            //SyncFixRagdollRoot();
        }

        private void OnDisable()
        {
            GoLimp();
        }

        private void OnEnable()
        {
            foreach (var jointData in AllJointData)
            {
                jointData.ResetJointAxisOnEnable(this);
            }

            GoFirm();
        }

        void FixedUpdate()
        {
            SyncRagdollUpdate();
            //SyncFixRagdollRoot();
            
            foreach (var jointData in AllJointData)
            {
                jointData.FixedUpdate();
            }
        }

        private void OnGUI()
        {
        }

#if UNITY_EDITOR
        [Activfy.Button]
        public void Setup()
        {
            if (!Application.isEditor || Application.isPlaying) return;

            AllJointData ??= new List<JointData>();
            _animatedParts ??= new List<Transform>();
            _ragdollParts ??= new List<Transform>();
            _jointDataDictionary ??= new Dictionary<Transform, JointData>();

            if (AllJointData.Count <= 0)
            {
                Link(AnimatedRoot, RagdollRoot);

                // Reset the values for the ragdoll's hips, it'll be handled in stability
                RagdollPelvis.JointLerpSpeed = 0;
                RagdollPelvis.Spring = 0;
                RagdollPelvis.Damper = 0;
            }
            else
            {
                Debug.LogWarning("The active ragdoll seems to have already been setup, please press reset if you wish to setup it up again.");
            }
        }

        [Activfy.Button]
        public void ResetActiveRagdoll()
        {
            if (!Application.isEditor || Application.isPlaying) return;

            AllJointData.Clear();
            _animatedParts.Clear();
            _ragdollParts.Clear();
            _jointDataDictionary.Clear();
        }

        [Activfy.Button]
        public void Remass()
        {
            if (!Application.isEditor || Application.isPlaying) return;

            RemassWizard.CreateWizard(this);
        }
        
        // [Activfy.Button]
        // public void NestJoints()
        // {
        //     if (!Application.isEditor || Application.isPlaying || AllJointData.Count == 0) return;
        //
        //     for (int i = 0; i < AllJointData.Count; i++)
        //     {
        //         JointData jointData = AllJointData[i];
        //
        //         Transform jointTrans = jointData.Rigid.transform;
        //
        //         if (jointTrans.parent.name.ToLower().Contains("_nest")) continue;
        //         
        //         GameObject nestObject = new GameObject($"{jointTrans.name}_nest");
        //         nestObject.transform.SetParent(jointTrans.parent);
        //         
        //         nestObject.transform.localPosition = Vector3.zero;
        //         nestObject.transform.localRotation = Quaternion.identity;
        //         nestObject.transform.localScale = Vector3.one;
        //         
        //         jointTrans.SetParent(nestObject.transform);
        //     }
        //     
        //     Debug.Log("[Activfi] Nested joints");
        // }
        //
        // [Activfy.Button]
        // public void UnNestJoints()
        // {
        //     for (int i = 0; i < AllJointData.Count; i++)
        //     {
        //         JointData jointData = AllJointData[i];
        //
        //         Transform jointTrans = jointData.Rigid.transform;
        //         Transform nestTrans = jointTrans.parent;
        //
        //         if (!nestTrans.name.ToLower().Contains("_nest")) continue;
        //         
        //         jointTrans.SetParent(nestTrans.parent);
        //         DestroyImmediate(nestTrans.gameObject);
        //     }
        //     
        //     Debug.Log("[Activfi] UnNested joints");
        // }

        /// <summary>
        /// Recursive linking
        /// </summary>
        /// <param name="animatedPart"></param>
        /// <param name="ragdollPart"></param>
        private void Link(Transform animatedPart, Transform ragdollPart)
        {
            if (!Application.isEditor) return;

            _animatedParts.Add(animatedPart);
            _ragdollParts.Add(ragdollPart);

            ConfigurableJoint configurableJoint = ragdollPart.GetComponent<ConfigurableJoint>();

            if (configurableJoint)
            {
                JointData jointData = new(this, configurableJoint, InWorldSpace ? ragdollPart.rotation : ragdollPart.localRotation, animatedPart);

                jointData.Spring = 200 * configurableJoint.GetComponent<Rigidbody>().mass;
                jointData.Damper = configurableJoint.GetComponent<Rigidbody>().mass;

                AllJointData.Add(jointData);
            }

            int aChildCount = animatedPart.childCount;
            int rChildCount = ragdollPart.childCount;

            if (aChildCount <= 0) return;

            for (int i = 0; i < aChildCount; i++)
            {
                for (int j = 0; j < rChildCount; j++)
                {
                    Transform aPart = animatedPart.GetChild(i);
                    Transform rPart = ragdollPart.GetChild(j);

                    if (!aPart.name.Equals(rPart.name)) continue;
                    Link(aPart, rPart);
                    break;
                }
            }
        }
#endif

        private void SyncRagdollUpdate()
        {
            if (_animatedParts.Count < 0 || !ShouldSync || (_stabilityController && _stabilityController.IsDown))
                return;

            for (int i = 0; i < _animatedParts.Count; i++)
            {
                Transform aPart = _animatedParts[i];
                Transform rPart = _ragdollParts[i];

                JointData jointData = null;

                if (_jointDataDictionary.ContainsKey(rPart))
                {
                    jointData = _jointDataDictionary[rPart];
                }

                //if (jointData == null) continue;

                // Handle hips in TurningUpdate
                if (jointData == RagdollPelvis)
                {
                    //Quaternion rotationTrue = Quaternion.AngleAxis(jointData.Joint.targetRotation.eulerAngles.y, Vector3.back) * aPart.transform.localRotation;

                    //jointData.Joint.SetTargetRotationLocal(rotationTrue, jointData.InitialQuat);

                    //continue;
                }

                if (jointData != null && jointData.Joint)
                {
                    if (!jointData.ShouldSync)
                        continue;

                    SyncWithPhysics(aPart, jointData, InWorldSpace);

                    // Apply some drag
                    ActivfiUtil.ApplyXZDrag(jointData.Rigid, 2.4f, Time.fixedDeltaTime);
                    ActivfiUtil.ApplyAngularDrag(jointData.Rigid, 1.0f, Time.fixedDeltaTime);
                }
                else
                    Sync(aPart, rPart);
            }
        }

        private static void Sync(Transform aPart, Transform rPart)
        {
            rPart.localRotation = aPart.localRotation; // rotation?
        }

        private static void SyncWithPhysics(Transform aPart, JointData jointData, bool worldSpace)
        {
            Rigidbody parentRigidbody = null;
            Quaternion parentInitialRotation = Quaternion.identity;

            if (jointData.Parent != null)
            {
                parentRigidbody = jointData.Parent.Rigid;
                parentInitialRotation = jointData.Parent.InitialQuat;
            }
            
            ActivfiUtil.SetTargetRotationOfJoint(jointData.Joint, aPart, jointData.InitialQuat, parentRigidbody, parentInitialRotation);
            
            // Should we lerp for correction?
            Rigidbody jointRigidbody = jointData.Rigid;
            // Rotate our transform a step closer to the target's.
            Quaternion stepRotation = Quaternion.RotateTowards(jointRigidbody.rotation, aPart.rotation, jointData.JointLerpSpeed * Time.deltaTime);

            jointRigidbody.MoveRotation(stepRotation);

            if (jointData.ShowDebug)
            {
                var jointPosition = jointData.Rigid.transform.position;
                Debug.DrawRay(jointPosition, aPart.forward, Color.magenta);
                Debug.DrawRay(jointPosition, jointData.Rigid.transform.forward, Color.red);
            }
        }

        public void UpdateJointSpace()
        {
            if (!Application.isEditor) return;

            foreach (JointData jointData in AllJointData)
            {
                jointData.Joint.configuredInWorldSpace = InWorldSpace;
            }
            
            for (int i = 0; i < AllJointData.Count; i++)
            {
                JointData jointData = AllJointData[i];

                jointData.InitialQuat = InWorldSpace ? jointData.Joint.transform.rotation : jointData.Joint.transform.localRotation;
            }
        }

        private void SyncFixRagdollRoot()
        {
            // Zero out on the ragdoll root
            transform.position += RagdollPelvis.Rigid.transform.position - transform.position;
            //RagdollRoot.rotation = RagdollPelvis.Rigid.rotation;
            Transform zeroOut = RagdollPelvis.Rigid.transform;
            do
            {
                zeroOut.localPosition = Vector3.zero;
                //zeroOut.localRotation = Quaternion.identity;
                zeroOut = zeroOut.parent;
            } while (zeroOut != null && zeroOut != transform);

            AnimatedRoot.position += RagdollPelvis.Rigid.transform.position - AnimatedPelvis.transform.position;

            //AnimatedRoot.eulerAngles = new Vector3(AnimatedRoot.eulerAngles.x, RagdollRoot);
        }

        private void UpdateSpringAndDamper()
        {
            for (int i = 0; i < AllJointData.Count; i++)
            {
                JointData jointData = AllJointData[i];

                jointData.UpdateSpringAndDamper();
            }
        }

        [Activfy.Button]
        public void GoLimp()
        {
            for (int i = 0; i < AllJointData.Count; i++)
            {
                AllJointData[i].GoLimp();
            }
        }

        [Activfy.Button]
        public void GoFirm()
        {
            for (int i = 0; i < AllJointData.Count; i++)
            {
                if (!DamageSystem || !DamageSystem.enabled || !DamageSystem.IsLimbImpaired(AllJointData[i]))
                    AllJointData[i].GoFirm();
            }
        }

        public void Lock()
        {
            // Disable physics interactions
            for (int i = 0; i < AllJointData.Count; i++)
            {
                Rigidbody rb = AllJointData[i].Rigid;
                rb.isKinematic = true;
            }
        }
        
        public void Unlock()
        {
            // Disable physics interactions
            for (int i = 0; i < AllJointData.Count; i++)
            {
                Rigidbody rb = AllJointData[i].Rigid;
                rb.isKinematic = false;
            }
        }

        public void Teleport(Vector3 teleportPosition, bool killVelocity = true)
        {
            Vector3 positionDiff = teleportPosition - RagdollPelvis.Rigid.position;

            // Perform teleportation
            for (int i = 0; i < AllJointData.Count; i++)
            {
                Rigidbody rb = AllJointData[i].Rigid;
                rb.MovePosition(rb.position + positionDiff);;
            }
        }
        
        /// <summary>
        /// When the joint is created, it takes the current position and rotation with respect to the Rigidbody as the joint’s rest pose. 
        /// That means that “zero position” and “zero angle” are the pose at the joint’s creation time.
        ///
        /// Joint limits are relative to the joint’s rest pose. For example, if some joint limit allows 2 degrees of rotation max, 
        /// this means 2 degrees with respect to the pose of the joint at the creation time.
        ///
        /// When the joint is disabled and re-enabled, the joint is re-created internally. 
        /// The rest pose is then reset to whatever position and rotation the Rigidbody has at the moment of re-creation. 
        /// The joint limits are the same (i.e., 2 degrees), but those limits are now with respect to the new pose.
        ///
        /// https://discussions.unity.com/t/bug-disabled-configurable-joints-changing-rotation-after-being-enabled/343643
        /// 
        /// </summary>
        public void Reset()
        {
            RagdollPelvis.Rigid.gameObject.SetActive(false);
            
            foreach (JointData jointData in AllJointData)
            {
                jointData.Rigid.transform.rotation = jointData.InitialQuat;
            }
        
            RagdollPelvis.Rigid.gameObject.SetActive(true);

        }
        
        public void SetVelocity(Vector3 velocity)
        {
            // Re-enable physics interactions
            for (int i = 0; i < AllJointData.Count; i++)
            {
                Rigidbody rb = AllJointData[i].Rigid;
                rb.velocity = velocity;
            }
        }
        
        public void SetVelocities(List<Vector3> velocities)
        {
            // Re-enable physics interactions
            for (int i = 0; i < AllJointData.Count; i++)
            {
                Rigidbody rb = AllJointData[i].Rigid;
                rb.velocity = velocities[i];
            }
        }
        
        public List<Vector3> GetVelocities()
        {
            List<Vector3> velocities = new List<Vector3>();
            
            // Re-enable physics interactions
            for (int i = 0; i < AllJointData.Count; i++)
            {
                Rigidbody rb = AllJointData[i].Rigid;
                velocities.Add(rb.velocity);
            }

            return velocities;
        }

        public Animator GetAnimator()
        {
            return AnimatedRoot.GetComponent<Animator>();
        }

        public Vector3 GetLocalJointDirection(VectorDirections direction, Transform joint)
        {
            Vector3 right = joint.right;
            Vector3 forward = joint.forward;
            Vector3 up = joint.up;

            return direction switch
            {
                VectorDirections.Forward => forward,
                VectorDirections.Back => -forward,
                VectorDirections.Left => -right,
                VectorDirections.Right => right,
                VectorDirections.Up => up,
                VectorDirections.Down => -up,
                _ => Vector3.zero
            };
        }

        public Vector3 GetLocalPelvisDirection(VectorDirections direction)
        {
            return GetLocalJointDirection(direction, RagdollPelvis.Rigid.transform);
        }
    }
}