using System;
using Activfy.Core;
using UnityEngine;

namespace Activfy.Examples.Scripts
{
    public class DemoManager : MonoBehaviour
    {
        public ActiveRagdoll ActiveRagdoll;
        public ActivfyStabilityController StabilityController;
        public float forceAmount = 500;

        Rigidbody selectedRigidbody;
        [SerializeField] Camera targetCamera;
        Vector3 originalScreenTargetPosition;
        Vector3 originalRigidbodyPos;
        float selectionDistance;
        [SerializeField] private Rigidbody _ball;
        [SerializeField] private float _ballThrowForce = 10f; // The force applied to the ball

        private void Awake()
        {
            StabilityController = ActiveRagdoll.GetComponent<ActivfyStabilityController>();
        }

        // Start is called before the first frame update
        void Start()
        {
            
        }

        void Update()
        {
            if (!targetCamera)
                return;

            if (Input.GetMouseButtonDown(0))
            {
                //Check if we are hovering over Rigidbody, if so, select it
                selectedRigidbody = GetRigidbodyFromMouseClick();
            }
            if (Input.GetMouseButtonUp(0) && selectedRigidbody)
            {
                //Release selected Rigidbody if there any
                selectedRigidbody = null;
            }

            if (Input.GetKey(KeyCode.L))
            {
                //ActiveRagdoll.GoLimp();
                StabilityController.Fall();
            }
            
            if (Input.GetKey(KeyCode.F))
            {
                //ActiveRagdoll.GoFirm();
                StabilityController.GetUp();
            }
            
            if (Input.GetKeyDown(KeyCode.B))
            {
                ThrowBallAtTarget();
            }
            
            if (Input.GetKeyDown(KeyCode.R))
            {
                ActiveRagdoll.RagdollPelvis.Rigid.transform.position = Vector3.up * 2f;
            }
        }
        
        private void ThrowBallAtTarget()
        {
            Vector3 direction = (ActiveRagdoll.RagdollPelvis.Rigid.position - _ball.position).normalized;
            _ball.velocity = direction * _ballThrowForce;
        }

        void FixedUpdate()
        {
            if (selectedRigidbody)
            {
                Vector3 mousePositionOffset = targetCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, selectionDistance)) - originalScreenTargetPosition;
                selectedRigidbody.velocity = (originalRigidbodyPos + mousePositionOffset - selectedRigidbody.transform.position) * (forceAmount * Time.deltaTime);
            }
        }

        Rigidbody GetRigidbodyFromMouseClick()
        {
            RaycastHit hitInfo = new RaycastHit();
            Ray ray = targetCamera.ScreenPointToRay(Input.mousePosition);
            bool hit = Physics.Raycast(ray, out hitInfo);
            if (hit)
            {
                if (hitInfo.collider.gameObject.GetComponent<Rigidbody>())
                {
                    selectionDistance = Vector3.Distance(ray.origin, hitInfo.point);
                    originalScreenTargetPosition = targetCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, selectionDistance));
                    originalRigidbodyPos = hitInfo.collider.transform.position;
                    return hitInfo.collider.gameObject.GetComponent<Rigidbody>();
                }
            }

            return null;
        }

        public void OnToggleStability()
        {
            StabilityController.enabled = !StabilityController.enabled;
        }
        
        public void OnToggleCanFall()
        {
            StabilityController.CanFall = !StabilityController.CanFall;
        }
    }
}
