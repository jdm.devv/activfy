using UnityEngine;

namespace Activfy.Examples.Scripts
{
    public class DemoCamera : MonoBehaviour
    {
        [SerializeField] private float moveSpeed;
        [SerializeField] private Transform target;
    
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            float hAxis = Input.GetAxis("Horizontal");
            
            transform.RotateAround(Vector3.zero, new Vector3(0.0f, -1.0f, 0.0f), 20 * Time.deltaTime * moveSpeed * hAxis);
            transform.LookAt(Vector3.up);
        }
    }
}
