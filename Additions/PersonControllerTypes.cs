using System;
using System.Collections.Generic;
using Activfy.Core;
using UnityEngine;

namespace Activfy.Additions
{
    public enum PersonControllerTypes
    {
        FirstPerson,
        ThirdPerson
    }
}