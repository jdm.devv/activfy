﻿using System;
using UnityEngine;

namespace Activfy.Additions
{
    public class DamageableLimb : MonoBehaviour, IDamageable
    {
        [ReadOnly] public ActivfyDamageSystem DamageSystem;
        [ReadOnly] public int LimbDataIndex;
        public DamageableLimbData LimbData => DamageSystem.DamageableLimbs[LimbDataIndex];

        public int MaxHealth
        {
            get => LimbData.MaxHealth;
            set { }
        }

        public int Health
        {
            get => LimbData.Health;
            set { }
        }

        public void TakeDamage(int amount)
        {
            int newHealth = Mathf.Clamp(Health - amount, 0, MaxHealth);

            LimbData.SetLimbHealth(newHealth);
        }
    }
}