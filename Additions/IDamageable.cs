﻿public interface IDamageable
{
    // Declare a Health property to represent the current health of the object
    int MaxHealth { get; set; }
    
    // Declare a Health property to represent the current health of the object
    int Health { get; set; }

    // Declare a method to handle taking damage
    void TakeDamage(int amount);
}