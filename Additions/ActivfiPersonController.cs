using System;
using Activfy.Additions;
using activfy.Additions.HandItems;
using Activfy.Core;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace activfy.Additions
{
    [AddComponentMenu("Activfy/Additions/Person Controller")]
    [RequireComponent(typeof(ActiveRagdoll), typeof(ActivfyMovementController), typeof(ActivfyStabilityController))]
    public class ActivfiPersonController : MonoBehaviour
    {
        private static readonly int _runningHash = Animator.StringToHash("IsRunning");
        private static readonly int _yMovementHash = Animator.StringToHash("YMovement");
        private static readonly int _xMovementHash = Animator.StringToHash("XMovement");
        private static readonly int _inAirHash = Animator.StringToHash("InAir");
        private static readonly int _getUpFront = Animator.StringToHash("GetUpFront");
        private static readonly int _getUpBack = Animator.StringToHash("GetUpBack");
        
        public PersonControllerTypes PersonControllerType;
        public bool IsRemotelyControlled = false;
        public bool PausedInputs = false;
        public Vector2 MovementInput;
        public Vector2 LookInput;
        public Vector2 SmoothedLookInput;
        
        [HideInInspector] public UnityEvent JumpEvent;
        [HideInInspector] public UnityEvent<GameObject, Vector3[]> GrabEvent;
        [HideInInspector] public UnityEvent<GameObject> PickupEvent;
        /// <summary>
        /// The event to drop/throw an item out of the persons hand
        /// @param GameObject - the object that is being dropped
        /// @param Vector3 - force being thrown with
        /// </summary>
        [HideInInspector] public UnityEvent<GameObject, Vector3> DropThrowEvent;
        [HideInInspector] public UnityEvent LetGoEvent;
        [HideInInspector] public UnityEvent<bool> IsRunningEvent;

        [SerializeField] private Camera _camera;
        [SerializeField] private Transform _firstPersonCameraSlot;
        [SerializeField] private Vector3 _thirdPersonOffset;
        [SerializeField] private float _thirdPersonCameraZoom = 5;
        [SerializeField] private Vector2 _verticalLookRange = new Vector2(-90, 90);
        [SerializeField] private float _jumpPower = 50;
        [SerializeField] private bool _isRunning = false;
        [SerializeField] private float _walkSpeed = 50;
        [SerializeField] private float _runSpeed = 150;

        [Header("Hand Items")]
        public Transform LeftHandItemSlot;
        public Transform RightHandItemSlot;
        public ActivfiHandItem LeftHandItem;
        public ActivfiHandItem RightHandItem;
        [SerializeField] private float _maxThrowForce = 10;
        [SerializeField] private float _throwForceChangeRate = 1;
        
        private float _currentThrowForce;

        [HideInInspector] public Transform SimulatedCameraHead;
        
        [Header("Aim")]
        public GameObject LookingAtGameObject;
        public GameObject DemoLookAimHit;
        public float InteractRange = 2.5f;

        private GameObject _interactProspect;
        public GameObject InteractProspect
        {
            get => _interactProspect;
            private set
            {
                GameObject previousInteractProspect = _interactProspect;
                
                _interactProspect = value;
                
                if (InteractProspect != previousInteractProspect)
                    InteractProspectChanged?.Invoke(previousInteractProspect);
            }
        }

        private RaycastHit _aimHit;

        /// <summary>
        /// @param is previous InteractProspect
        /// </summary>
        [HideInInspector] public UnityEvent<GameObject> InteractProspectChanged = new UnityEvent<GameObject>();
        
        private ActivfyMovementController _movementController;
        private ActivfyStabilityController _stabilityController;
        private ActivfyGrabController _grabController;
        private ActiveRagdoll _connectedActiveRagdoll;
        private Animator _animator;

        private void Awake()
        {
            _connectedActiveRagdoll = GetComponent<ActiveRagdoll>();
            _movementController = GetComponent<ActivfyMovementController>();
            _stabilityController = GetComponent<ActivfyStabilityController>();
            _grabController = GetComponent<ActivfyGrabController>();
            _animator = _connectedActiveRagdoll.GetAnimator();

            if (!_camera)
            {
                _camera = Camera.main;
            }

            SimulatedCameraHead = new GameObject("SimulatedCameraHead").transform;
            SimulatedCameraHead.SetParent(transform);
            SimulatedCameraHead.localPosition = Vector3.zero;
            
            _stabilityController.OnGetUpFront.AddListener(() =>
            {
                _animator.SetTrigger(_getUpFront);
            });
            
            _stabilityController.OnGetUpBack.AddListener(() =>
            {
                _animator.SetTrigger(_getUpBack);
            });
        }

        // Start is called before the first frame update
        void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            
            JumpEvent.AddListener(Jump);
            LetGoEvent.AddListener(() =>
            {
                _grabController.RemoveGrab();
            });
            GrabEvent.AddListener((gameObjectToGrab, grabData) =>
            {
                _grabController.Grab(gameObjectToGrab, grabData);
            });
            IsRunningEvent.AddListener(isRunning =>
            {
                _isRunning = isRunning;
            });
            PickupEvent.AddListener(gameObjectToPickup =>
            {
                if (gameObjectToPickup.TryGetComponent(out ActivfiHandItem hitHandItem))
                    ActivfiHandItem.TryPutItemInHandJoint(this, hitHandItem);
            });
            DropThrowEvent.AddListener((handItemGameObject, throwForce) =>
            {
                ActivfiHandItem.RemoveItemInHandJoint(this, handItemGameObject.GetComponent<ActivfiHandItem>().HandItemType, throwForce, true);
            });
        }

        // Update is called once per frame
        void Update()
        {
            if (!PausedInputs)
            {
                InputsUpdate();
            }
            else
            {
                MovementInput = Vector2.zero;
                LookInput = Vector2.zero;
            }

            CalculateAimHit();
            
            //Debug.DrawLine(GetRagdollPelvis().position, GetRagdollPelvis().position + GetTrueAimLookDirection(GetRagdollPelvis().transform) * 20f, Color.white);
            
            SmoothedLookInput = Vector2.Lerp(SmoothedLookInput, LookInput, Time.deltaTime * 40f);
            
            MovementUpdate();
            if (PersonControllerType == PersonControllerTypes.FirstPerson)
            {
                LookUpdateFirstPerson();
            } else
            {
                LookUpdateThirdPerson();
            }
            PhysicalUpdate();

            // must be done after updates above
            if (!IsRemotelyControlled)
            {
                _camera.transform.position = SimulatedCameraHead.position;
                _camera.transform.rotation = SimulatedCameraHead.rotation;
            }
        }

        private void OnDisable()
        {
            InteractProspect = null;
            LookingAtGameObject = null;
        }

        private void InputsUpdate()
        {
            if (!IsRemotelyControlled)
            {
                UpdateLookingAtGameObject();
                
                MovementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
                LookInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    JumpEvent?.Invoke();
                }
                
                if (Input.GetMouseButtonDown(1))
                {
                    UpdateLookingAtGameObject();
                    
                    if(LookingAtGameObject)
                    {
                        GrabEvent?.Invoke(LookingAtGameObject, _grabController.GetGrabData(LookingAtGameObject));
                    }
                }

                if (Input.GetMouseButtonUp(1))
                {
                    LetGoEvent?.Invoke();
                }
                
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    IsRunningEvent?.Invoke(true);
                }
                
                if (Input.GetKeyUp(KeyCode.LeftShift))
                {
                    IsRunningEvent?.Invoke(false);
                }
                
                if (Input.GetKeyDown(KeyCode.F))
                {
                    if(InteractProspect && InteractProspect.TryGetComponent(out ActivfiHandItem hitHandItem))
                    {
                        PickupEvent?.Invoke(InteractProspect);
                    }
                }

                if (Input.GetKey(KeyCode.Q))
                {
                    // Throwing force add to
                    _currentThrowForce = Mathf.Clamp(_currentThrowForce + Time.deltaTime * _throwForceChangeRate, 0f, _maxThrowForce);
                }
                
                if (Input.GetKeyUp(KeyCode.Q))
                {
                    ActivfiHandItem handItem = null;
                    if (LeftHandItem)
                    {
                        handItem = LeftHandItem;
                    } 
                    else if (RightHandItem)
                    {
                        handItem = RightHandItem;
                    }

                    if (handItem)
                    {
                        DropThrowEvent?.Invoke(handItem.gameObject, GetTrueAimLookDirection(handItem.transform) * _currentThrowForce);
                    }

                    _currentThrowForce = 0;
                }
                
                if (Input.GetKeyDown(KeyCode.F5))
                {
                    PersonControllerType = (PersonControllerTypes)(((int)PersonControllerType + 1) % Enum.GetValues(typeof(PersonControllerTypes)).Length);
                }
                
                if (Input.GetKeyDown(KeyCode.F1))
                {
                    Vector3 position = _connectedActiveRagdoll.RagdollPelvis.Rigid.position;
                    position.y = 5;
                    position.x = 4;
                    position.z = 3;
                    
                    _connectedActiveRagdoll.Teleport(position);
                    
                    Debug.Log(_connectedActiveRagdoll.RagdollPelvis.Rigid.position);
                }
            }
        }

        private void MovementUpdate()
        {
            float hAxis = MovementInput.x;
            float vAxis = MovementInput.y;

            Vector3 hipsForward = SimulatedCameraHead.forward * vAxis;
            hipsForward.y = 0;

            Vector3 hipsRight = SimulatedCameraHead.right * hAxis;
            hipsRight.y = 0;

            // Combine forward and right movements
            Vector3 movementVector = hipsForward + (hipsRight / 2f);

            Vector3 movementDirection = movementVector.normalized;

            if (movementDirection == Vector3.zero)
            {
                //movementDirection = _connectedActiveRagdoll.RagdollPelvis.Rigid.transform.forward.normalized; ?
            }
            
            // Apply
            _movementController.MovementModifer = Mathf.Max(Mathf.Abs(hAxis), Mathf.Abs(vAxis));
            _movementController.SetMoveDirection(movementDirection);
            _movementController.Acceleration = _movementController.MaxMoveSpeed = _isRunning ? _runSpeed : _walkSpeed;
            
            // Animation
            Vector2 inputDirection = new Vector2(hAxis, vAxis);
            _animator.SetFloat(_xMovementHash, inputDirection.x);
            _animator.SetFloat(_yMovementHash, inputDirection.y);
            _animator.SetBool(_runningHash, _isRunning);
            _animator.SetBool(_inAirHash, _stabilityController.IsAirborne());
        }

        private void LookUpdateFirstPerson()
        {
            float mouseX = SmoothedLookInput.x;
            float mouseY = SmoothedLookInput.y;

            // Adjust these values to control the sensitivity of the mouse input
            float lookSensitivity = 5.0f;

            // Calculate the yaw (horizontal) and pitch (vertical) rotations
            float yaw = mouseX * lookSensitivity;
            float pitch = -mouseY * lookSensitivity;

            // Get the current local rotation of the camera
            Vector3 currentEulerAngles = SimulatedCameraHead.localEulerAngles;
    
            // Adjust the yaw (horizontal) rotation
            currentEulerAngles.y += yaw;

            // Adjust the pitch (vertical) rotation and clamp it to prevent flipping
            currentEulerAngles.x += pitch;
            if (currentEulerAngles.x > 180)
            {
                currentEulerAngles.x -= 360;
            }
            currentEulerAngles.x = Mathf.Clamp(currentEulerAngles.x, _verticalLookRange.x, _verticalLookRange.y);

            // Apply the new rotation to the camera
            SimulatedCameraHead.localEulerAngles = currentEulerAngles;

            Rigidbody ragdollRigidbody = _connectedActiveRagdoll.RagdollPelvis.Rigid;
            
            Vector3 currentPosition = SimulatedCameraHead.position;
            
            Vector3 hardTargetPosition = _firstPersonCameraSlot.transform.position;
            Vector3 difference = hardTargetPosition - ragdollRigidbody.position;
            Vector3 softTargetPosition = ragdollRigidbody.position + new Vector3(0, difference.y, 0);

            Vector3 velocity = ragdollRigidbody.velocity;
            currentPosition = Vector3.SmoothDamp(currentPosition, hardTargetPosition, ref velocity, Time.deltaTime);

            SimulatedCameraHead.position = hardTargetPosition;
        }

        private void LookUpdateThirdPerson()
        {
            float mouseX = LookInput.x;
            float mouseY = LookInput.y;

            // Adjust these values to control the sensitivity of the mouse input
            float lookSensitivity = 5.0f;

            // Calculate the yaw (horizontal) and pitch (vertical) rotations
            float yaw = mouseX * lookSensitivity;
            float pitch = -mouseY * lookSensitivity;

            // Get the current local rotation of the camera
            Vector3 currentEulerAngles = SimulatedCameraHead.localEulerAngles;

            // Adjust the yaw (horizontal) rotation
            currentEulerAngles.y += yaw;

            // Adjust the pitch (vertical) rotation and clamp it to prevent flipping
            currentEulerAngles.x += pitch;
            if (currentEulerAngles.x > 180)
            {
                currentEulerAngles.x -= 360;
            }
            currentEulerAngles.x = Mathf.Clamp(currentEulerAngles.x, _verticalLookRange.x, _verticalLookRange.y);

            // Apply the new rotation to the camera
            SimulatedCameraHead.localEulerAngles = currentEulerAngles;

            // Calculate the pivot position based on the pelvis and offset
            Vector3 pivotPosition = _connectedActiveRagdoll.RagdollPelvis.Rigid.transform.position + _thirdPersonOffset;

            // Calculate the camera's new position based on the pivot and zoom level
            Vector3 zoomedPosition = pivotPosition - SimulatedCameraHead.forward * _thirdPersonCameraZoom;

            // Apply the new position to the camera
            SimulatedCameraHead.position = zoomedPosition;
        }

        public void UpdateLookingAtGameObject()
        {
            Debug.DrawRay(_camera.transform.position, GetLookDirection());
            
            if (Physics.Raycast(_camera.transform.position, GetLookDirection(), out var hit, Mathf.Infinity))
            {
                LookingAtGameObject = hit.collider.gameObject;

                if (hit.distance < InteractRange)
                {
                    InteractProspect = CanInteractWith(hit.collider.gameObject) ? LookingAtGameObject : null;
                }
                else
                {
                    InteractProspect = null;
                }

                return;
            }

            InteractProspect = null;
            LookingAtGameObject = null;
        }

        public bool CanInteractWith(GameObject interactableObject)
        {
            bool canInteract = interactableObject.GetComponent<Rigidbody>() || 
                               interactableObject.layer == LayerMask.NameToLayer("Interactable");

            canInteract = canInteract &&
                          interactableObject != LeftHandItem?.gameObject &&
                          interactableObject != RightHandItem?.gameObject &&
                          interactableObject != _grabController?.GetGrabGameObject();

            if (canInteract)
            {
                ActivfiPersonController parentPersonController = interactableObject.GetComponentInParent<ActivfiPersonController>();

                if (parentPersonController && parentPersonController == this)
                    canInteract = false;
            }

            return canInteract;
        }

        public Rigidbody GetRagdollPelvis()
        {
            return _connectedActiveRagdoll.RagdollPelvis.Rigid;
        }

        public Vector3 GetLookDirection()
        {
            return SimulatedCameraHead.forward;
        }

        public Vector3 GetTrueAimLookDirection(Transform self)
        {
            Vector3 direction = GetLookDirection();

            RaycastHit aimHit = GetAimHit();
            if (self && aimHit.transform)
            {
                direction = (aimHit.point - self.position).normalized;
            }
            
            return direction;
        }

        public void CalculateAimHit(float aimDistance = 1000f)
        {
            if (Physics.Raycast(SimulatedCameraHead.position, GetLookDirection(), out RaycastHit hit, aimDistance))
            {
                _aimHit = hit;
                return;
            }
            
            _aimHit = default(RaycastHit);
        }
        
        public RaycastHit GetAimHit()
        {
            return _aimHit;
        }
        
        public Vector3 GetAimDirection(Transform aimTransform, float aimDistance = 1000f)
        {
            Vector3 direction = GetLookDirection();

            if (Physics.Raycast(SimulatedCameraHead.position, direction, out RaycastHit hit, aimDistance))
            {
                direction = (hit.point - aimTransform.position).normalized;
            }

            return direction;
        }
        
        public float GetThrowPercent()
        {
            return _currentThrowForce / _maxThrowForce;
        }

        private void PhysicalUpdate()
        {
            // This needs to be evaluated later TODO
            if (!_stabilityController.IsDown)
            {
                // Extract the yaw from the camera's rotation and apply it to the AnimatedRoot parent
                Quaternion cameraRotation = SimulatedCameraHead.rotation;
                Vector3 cameraEulerAngles = cameraRotation.eulerAngles;
                Quaternion yawRotation = Quaternion.Euler(0, cameraEulerAngles.y, 0);
                
                _connectedActiveRagdoll.AnimatedRoot.rotation = Quaternion.RotateTowards(_connectedActiveRagdoll.AnimatedRoot.rotation, yawRotation, Time.deltaTime * 380f);
            }
        }

        private void Jump()
        {
            _connectedActiveRagdoll.RagdollPelvis.Rigid.AddForce(Vector3.up * _jumpPower, ForceMode.Impulse);
            _stabilityController.SetIsFallImmuneTillGrounded(true);
        }
    }
}