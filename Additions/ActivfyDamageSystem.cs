﻿using System;
using System.Collections.Generic;
using Activfy.Core;
using UnityEngine;

namespace Activfy.Additions
{
    [AddComponentMenu("Activfy/Additions/Damage System")]
    [RequireComponent(typeof(ActiveRagdoll))]
    public class ActivfyDamageSystem : MonoBehaviour
    {
        [SerializeReference] public List<DamageableLimbData> DamageableLimbs = new List<DamageableLimbData>();
        
        [ReadOnly, Range(0.0f, 1.0f)] public float Conciseness;
        [ReadOnly] public float TotalMovementModify;
        [ReadOnly] public ActiveRagdoll connectedActiveRagdoll;

        private void Awake()
        {
            connectedActiveRagdoll = GetComponent<ActiveRagdoll>();
        }

        private void Start()
        {
            RecalculateMovementModifier();
        }

        private void Update()
        {
            
        }

        [Activfy.Button]
        private void Setup()
        {
            if (!Application.isEditor) return;
            
            connectedActiveRagdoll = GetComponent<Core.ActiveRagdoll>();

            if (DamageableLimbs.Count > 0)
            {
                Debug.LogError("DamageableLimbs already set, please reset this before trying to setup again");
                return;
            }
            
            if (connectedActiveRagdoll.AllJointData.Count > 0)
            {
                for (int i = 0; i < connectedActiveRagdoll.AllJointData.Count; i++)
                {
                    JointData jointData = connectedActiveRagdoll.AllJointData[i];

                    DamageableLimbData limbData = new(this,
                                                      jointData.Rigid.gameObject.AddComponent<DamageableLimb>(),
                                                      jointData,
                                                      i);

                    DamageableLimbs.Add(limbData);
                }
            }
            else
            {
                Debug.LogError("Andy isn't setup with any joints yet, go ahead and set that up before setting up the AndyDamageSystem");
                return;
            }
        }
        
        [Activfy.Button]
        private void ResetSystem()
        {
            if (!Application.isEditor) return;

            for (int i = 0; i < DamageableLimbs.Count; i++)
            {
                DamageableLimbs[i].CleanUp();
            }
            
            DamageableLimbs.Clear();

            DamageableLimb[] extraDamageableLimbs = GetComponentsInChildren<DamageableLimb>();

            if (extraDamageableLimbs.Length > 0)
            {
                for (int i = 0; i < extraDamageableLimbs.Length; i++)
                {
                    DestroyImmediate(extraDamageableLimbs[i]);
                }
            }

            Debug.Log("Reset complete");
        }

        public DamageableLimbData GetDamageableLimbData(JointData jointData)
        {
            for (int i = 0; i < DamageableLimbs.Count; i++)
            {
                if (DamageableLimbs[i].Joint.Equals(jointData))
                    return DamageableLimbs[i];
            }

            return null;
        }
        
        public bool IsLimbImpaired(JointData jointData)
        {
            DamageableLimbData limbData = GetDamageableLimbData(jointData);
            return limbData is { IsImpaired: true };
        }

        public void RecalculateMovementModifier()
        {
            float total = 1.0f;
            for (int i = 0; i < DamageableLimbs.Count; i++)
            {
                DamageableLimbData limbData = DamageableLimbs[i];

                float percentApply = limbData.MovementMultiplier * (1.0f - ((float)limbData.Health / limbData.MaxHealth)); // 0-1
                
                total *= 1.0f - percentApply;
            }

            TotalMovementModify = total;
        }
    }
}