﻿using System;
using Activfy.Additions;
using Activfy.Core;
using Activfy.Helpers;
using UnityEngine;

namespace activfy.Additions
{
    public class ActivfySteeringMovementController : ActivfyMovementController
    {
        public Transform PracticeTarget;
        public Vector3 TargetDirection = Vector3.right;
        public float StoppingDistance = 1;
        [ReadOnly] public float CurrentDistance = 0;
        
        private new void FixedUpdate()
        {
            if (StabilityController && StabilityController.IsDown) return;
            
            if (PracticeTarget)
            {
                float distanceToTarget = Vector3.Distance(ConnectedActiveRagdoll.RagdollPelvis.Rigid.transform.position, PracticeTarget.position);

                CurrentDistance = distanceToTarget;
                
                if (distanceToTarget < StoppingDistance)
                    return;
            }
            
            // Set move direction to go forward
            MoveDirection = ConnectedActiveRagdoll.GetLocalPelvisDirection(ConnectedActiveRagdoll.PelvisForward);
            MoveDirection.y = 0;

            TurningUpdate();
            MovementUpdate();
        }
        
        private void TurningUpdate()
        {
            if (PracticeTarget)
            {
                TargetDirection = (PracticeTarget.position - RagdollHips.Rigid.position).normalized;
            }
            
            float angle = Mathf.Atan2(TargetDirection.x, TargetDirection.z) * Mathf.Rad2Deg;
            
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.up);
            ConnectedActiveRagdoll.AnimatedRoot.rotation = q; 
            
            Debug.DrawRay(RagdollHips.Rigid.position, TargetDirection);
        }

        public void FollowTarget(GameObject target)
        {
            PracticeTarget = target.transform;
        }
    }
}