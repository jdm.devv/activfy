using Activfy.Core;
using UnityEngine;

namespace activfy.Additions
{
    public class ActivfiPoseAligner : MonoBehaviour
    {
        public Transform PelvisAlignTo;
        public string AnimationName;
        [ReadOnly] public string UnAlignTrigger = "UnAlign";
        /// <summary>
        /// Can rotate x degrees each side
        /// </summary>
        public float RotateRange = 45;
        
        private ActiveRagdoll _activeRagdoll;
    
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                if (_activeRagdoll)
                {
                    UnAlign();
                }
                else
                {
                    PoseAlign(FindObjectOfType<ActiveRagdoll>());
                }
            }

            if (_activeRagdoll)
            {
                Debug.Log(_activeRagdoll.name);
                Rigidbody pelvisRigidbody = _activeRagdoll.RagdollPelvis.Rigid;
                Vector3 positionDifference = PelvisAlignTo.position - pelvisRigidbody.position;
                Vector3 step = positionDifference * (Time.deltaTime * 2.0f);
                _activeRagdoll.Teleport(pelvisRigidbody.position + step);
                
                Quaternion rotationDifference = Quaternion.Inverse(pelvisRigidbody.rotation) * PelvisAlignTo.rotation;
                Quaternion stepRotation = Quaternion.Slerp(Quaternion.identity, rotationDifference, Time.deltaTime * 2.0f);
                pelvisRigidbody.MoveRotation(pelvisRigidbody.rotation * stepRotation);
            }
        }

        public void PoseAlign(ActiveRagdoll ragdoll)
        {
            _activeRagdoll = ragdoll;
            
            Animator animator = ragdoll.GetAnimator();
            animator.Play(AnimationName);
        }

        public void UnAlign()
        {
            Animator animator = _activeRagdoll.GetAnimator();
            animator.SetTrigger(UnAlignTrigger);
            
            _activeRagdoll = null;
        }
    }
}
