﻿using System;
using Activfy.Core;
using UnityEngine;
using UnityEngine.Events;

namespace Activfy.Additions
{
    [Serializable]
    public class DamageableLimbData
    {
        [ReadOnly, SerializeField] private string _name;
        [ReadOnly, SerializeField] private int _limbDataIndex;
        
        [ReadOnly] public ActivfyDamageSystem CoreSystem;
        [ReadOnly] public DamageableLimb Limb;
        [ReadOnly, SerializeReference] public JointData Joint;

        [Header("Survivability")]
        public int MaxHealth;
        public int Health;
        [Header("Dynamic Elements")]
        [ReadOnly, SerializeField] private bool _isImpaired;
        [Range(0.0f, 1.0f)] public float MovementMultiplier;

        public bool IsImpaired => _isImpaired;

        [Header("Events")]
        public LimbDamageEvent OnLimbDamage;
        public LimbImpairedEvent OnLimbImpaired;

        public DamageableLimbData(ActivfyDamageSystem coreSystem, DamageableLimb limbComponent, JointData jointData, int limbDataIndex)
        {
            CoreSystem = coreSystem;
            Limb = limbComponent;
            Joint = jointData;
            MovementMultiplier = 0.0f;
            Health = 10;
            MaxHealth = 10;

            _name = limbComponent.gameObject.name;
            limbComponent.DamageSystem = coreSystem;

            Limb.LimbDataIndex = _limbDataIndex = limbDataIndex;
            
            _isImpaired = false;
            OnLimbDamage = null;
            OnLimbImpaired = null;
        }

        public void CleanUp()
        {
            if (!Application.isEditor) return;
            
            UnityEngine.Object.DestroyImmediate(Limb);
        }

        public void SetLimbHealth(int newHealth)
        {
            int changeInHealth = newHealth - Health;
            Health = Mathf.Clamp(Health + changeInHealth, 0, MaxHealth);

            if (changeInHealth < 0)
                OnLimbDamage?.Invoke(changeInHealth);

            switch (Health)
            {
                case 0 when !_isImpaired:
                    ImpairLimb();
                    break;
                case > 0 when _isImpaired:
                    UnImpairLimb();
                    break;
            }
            
            CoreSystem.RecalculateMovementModifier();
        }

        public void ImpairLimb()
        {
            _isImpaired = true;
            OnLimbImpaired?.Invoke();
            
            Joint.GoLimp();
        }
        
        public void UnImpairLimb()
        {
            _isImpaired = false;

            Joint.GoFirm();
        }
    }
    
    [Serializable]
    public class LimbDamageEvent : UnityEvent<float> {}
    
    [Serializable]
    public class LimbImpairedEvent : UnityEvent {}
}