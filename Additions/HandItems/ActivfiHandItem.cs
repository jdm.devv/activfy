﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace activfy.Additions.HandItems
{
    public class ActivfiHandItem : MonoBehaviour
    {
        /// <summary>
        /// Where on the item the person will hold the item
        /// </summary>
        public Transform HandGrabSlot;
        /// <summary>
        /// What is the dominant hand to use
        /// </summary>
        public HandItemType HandItemType;
        /// <summary>
        /// (optional)
        /// </summary>
        public Transform OtherHandIKTarget;

        public bool CanPickUp = true;
        
        [Header("Joint Data")]
        public Vector3 Axis = Vector3.right;
        public Vector3 SecondaryAxis = Vector3.up;
        public Vector2 AngularXLimit = Vector3.zero;
        public float AngularYLimit = 0.0f;
        public float AngularZLimit = 0.0f;

        /// <summary>
        /// The temporary mass being held until it is dropped
        /// </summary>
        private float _tempHoldMass;

        private bool _beingHeld;
        
        /// <summary>
        /// The temporary mass being held until it is dropped
        /// </summary>
        private ConfigurableJoint _joint;

        private Rigidbody _rigidbody;
        
        [HideInInspector] public UnityEvent<Vector3> OnItemDropThrow = new UnityEvent<Vector3>();

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public bool IsItemTwoHanded()
        {
            return OtherHandIKTarget;
        }

        // public static void TryPutItemInHand(ActivfiPersonController personController, ActivfiHandItem handItem)
        // {
        //     // Make sure hand is empty
        //     if (!IsHandEmpty(personController, handItem.HandItemType))
        //         return;
        //
        //     switch (handItem.HandItemType)
        //     {
        //         case HandItemType.LeftHand:
        //             personController.LeftHandItem = handItem;
        //             PutItemInHandSlot(personController.LeftHandItemSlot, handItem);
        //             break;
        //         case HandItemType.RightHand:
        //             personController.RightHandItem = handItem;
        //             PutItemInHandSlot(personController.RightHandItemSlot, handItem);
        //             break;
        //         default:
        //             throw new ArgumentOutOfRangeException();
        //     }
        // }
        
        public static void TryPutItemInHandJoint(ActivfiPersonController personController, ActivfiHandItem handItem)
        {
            if (!handItem.CanPickUp)
                return;
            
            // Make sure hand is empty
            if (!IsHandEmpty(personController, handItem.HandItemType))
                return;
            
            // Only can hold if no one else is using
            if (handItem._beingHeld)
                return;

            switch (handItem.HandItemType)
            {
                case HandItemType.LeftHand:
                    personController.LeftHandItem = handItem;
                    PutItemInHandJoint(personController.LeftHandItemSlot, handItem);
                    break;
                case HandItemType.RightHand:
                    personController.RightHandItem = handItem;
                    PutItemInHandJoint(personController.RightHandItemSlot, handItem);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        private static void PutItemInHandJoint(Transform handSlot, ActivfiHandItem handItem)
        {
            handItem._tempHoldMass = handItem._rigidbody.mass;
            handItem._rigidbody.mass = 0;
            
            // Calculate the rotation offset relative to the hand slot (without parenting)
            Quaternion grabRotationOffset = Quaternion.Inverse(handItem.transform.rotation) * handItem.HandGrabSlot.rotation;

            // Apply the relative rotation to the hand slot
            handItem.transform.rotation = handSlot.transform.rotation * Quaternion.Inverse(grabRotationOffset);

            // Calculate the position offset relative to the hand slot (without parenting)
            Vector3 grabPositionOffset = handItem.transform.position - handItem.HandGrabSlot.position;

            // Apply the relative position to the hand slot
            handItem.transform.position = handSlot.transform.position + grabPositionOffset;

            ConfigurableJoint configurableJoint = handItem.gameObject.AddComponent<ConfigurableJoint>();
            configurableJoint.connectedBody = handSlot.GetComponentInParent<Rigidbody>();
            configurableJoint.anchor = handItem.transform.InverseTransformPoint(handItem.HandGrabSlot.position);

            configurableJoint.xMotion = configurableJoint.yMotion = configurableJoint.zMotion = ConfigurableJointMotion.Locked;
            configurableJoint.angularXMotion = configurableJoint.angularYMotion = configurableJoint.angularZMotion = ConfigurableJointMotion.Limited;
            configurableJoint.axis = handItem.Axis;
            configurableJoint.secondaryAxis = handItem.SecondaryAxis;
            
            // Low X angular limit
            SoftJointLimit limit = configurableJoint.lowAngularXLimit;
            limit.limit = handItem.AngularXLimit.x;
            configurableJoint.lowAngularXLimit = limit;
            
            // High X angular limit
            limit = configurableJoint.highAngularXLimit;
            limit.limit = handItem.AngularXLimit.y;
            configurableJoint.highAngularXLimit = limit;

            // Y angular limit
            limit = configurableJoint.angularYLimit;
            limit.limit = handItem.AngularYLimit;
            configurableJoint.angularYLimit = limit;
            
            // Z angular limit
            limit = configurableJoint.angularZLimit;
            limit.limit = handItem.AngularZLimit;
            configurableJoint.angularZLimit = limit;

            handItem._joint = configurableJoint;
            
            handItem._beingHeld = true;
        }
        
        public static ActivfiHandItem RemoveItemInHandJoint(ActivfiPersonController personController, HandItemType hand, Vector3 throwForce = default, bool killVelocity = false)
        {
            ActivfiHandItem handItem = null;

            if (personController.LeftHandItem && hand == HandItemType.LeftHand)
            {
                handItem = personController.LeftHandItem;
                personController.LeftHandItem = null;
            }
            else if (personController.RightHandItem && hand == HandItemType.RightHand)
            {
                handItem = personController.RightHandItem;
                personController.RightHandItem = null;
            }
            else
            {
                return null;
            }
            
            DestroyImmediate(handItem._joint);
            handItem._rigidbody.mass = handItem._tempHoldMass;
            handItem._tempHoldMass = 0;
            
            if (killVelocity)
                handItem._rigidbody.velocity = Vector3.zero;

            handItem._rigidbody.velocity += throwForce;
            
            handItem.OnItemDropThrow?.Invoke(throwForce);

            handItem._beingHeld = false;
            
            return handItem;
        }

        // private static void PutItemInHandSlot(Transform handSlot, ActivfiHandItem handItem)
        // {
        //     if (handItem.TryGetComponent(out Rigidbody rigidbody))
        //     {
        //         handItem._tempHoldMass = rigidbody.mass;
        //         //rigidbody.mass = 0;
        //         rigidbody.isKinematic = true;
        //         handItem.GetComponent<Collider>().enabled = false;
        //     }
        //
        //     Quaternion grabRotationOffset = Quaternion.Inverse(handItem.transform.rotation) * handItem.HandGrabSlot.rotation;
        //     handItem.transform.SetParent(handSlot, true);
        //     handItem.transform.localRotation = Quaternion.Inverse(grabRotationOffset);
        //     
        //     Vector3 grabPositionOffset = handItem.transform.position - handItem.HandGrabSlot.position;
        //     handItem.transform.position = handSlot.transform.position + grabPositionOffset;
        // }
        
        // public static ActivfiHandItem RemoveItemInHand(ActivfiPersonController personController, HandItemType hand)
        // {
        //     return null;
        // }
        
        public static bool IsHandEmpty(ActivfiPersonController personController, HandItemType hand)
        {
            return !GetRealItemInHand(personController, hand);
        }
        
        /// <summary>
        /// Will return what item is physically in hand (this mean 2 handed items return for both left and right hands)
        /// </summary>
        /// <param name="personController"></param>
        /// <param name="hand"></param>
        /// <returns></returns>
        public static ActivfiHandItem GetRealItemInHand(ActivfiPersonController personController, HandItemType hand)
        {
            // Two-handed checks
            if (personController.LeftHandItem && personController.LeftHandItem.IsItemTwoHanded())
                return personController.LeftHandItem;
            if (personController.RightHandItem && personController.RightHandItem.IsItemTwoHanded())
                return personController.RightHandItem;
            
            if (personController.LeftHandItem && hand == HandItemType.LeftHand)
                return personController.LeftHandItem;
            if (personController.RightHandItem && hand == HandItemType.RightHand)
                return personController.RightHandItem;

            return null;
        }
    }

    public enum HandItemType
    {
        LeftHand,
        RightHand
    }
}