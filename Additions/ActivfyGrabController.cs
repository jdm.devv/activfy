using System.Collections;
using System.Collections.Generic;
using activfy.Additions;
using Activfy.Core;
using UnityEngine;

[RequireComponent(typeof(ActiveRagdoll))]
public class ActivfyGrabController : MonoBehaviour
{
    [SerializeField] private ConfigurableJoint _leftHandJoint;
    [SerializeField] private ConfigurableJoint _rightHandJoint;
    [SerializeField] private Transform _leftHandPalm;
    [SerializeField] private Transform _rightHandPalm;

    private List<ConfigurableJoint> _grabbedJoints = new List<ConfigurableJoint>();
    private GameObject _grabbedObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetGrabGameObject()
    {
        return _grabbedObject;
    }

    public Vector3[] GetGrabData(GameObject gameObjectToGrab)
    {
        // Find the closest point on the collider to the hand palm
        Vector3 leftHandPlacement = gameObjectToGrab.GetComponent<Collider>().ClosestPoint(_leftHandPalm.position);
        Vector3 rightHandPlacement = gameObjectToGrab.GetComponent<Collider>().ClosestPoint(_rightHandPalm.position);
        
        // Calculate the position difference from the hand's palm to the closest point on the object
        Vector3 leftLocalGrabPoint = gameObjectToGrab.transform.InverseTransformPoint(leftHandPlacement);
        Vector3 rightLocalGrabPoint = gameObjectToGrab.transform.InverseTransformPoint(rightHandPlacement);
        
        return new[]
        {
            _leftHandJoint.transform.InverseTransformPoint(_leftHandPalm.position),
            _rightHandJoint.transform.InverseTransformPoint(_rightHandPalm.position),
            leftLocalGrabPoint,
            rightLocalGrabPoint
        };
    }

    public void Grab(GameObject gameObjectToGrab)
    {
        Vector3[] grabData = GetGrabData(gameObjectToGrab);
        
        HandGrab(_leftHandJoint, _leftHandPalm, gameObjectToGrab, grabData[0], grabData[2]);
        HandGrab(_rightHandJoint, _rightHandPalm, gameObjectToGrab, grabData[1], grabData[3]);
    }
    
    public void Grab(GameObject gameObjectToGrab, Vector3[] grabData)
    {
        HandGrab(_leftHandJoint, _leftHandPalm, gameObjectToGrab, grabData[0], grabData[2]);
        HandGrab(_rightHandJoint, _rightHandPalm, gameObjectToGrab, grabData[1], grabData[3]);
    }

    private void HandGrab(ConfigurableJoint handJoint, Transform handPalm, GameObject gameObjectToGrab, Vector3 connectedAnchorHandPalm, Vector3 anchorHandPalm)
    {
        Rigidbody rbToGrab = gameObjectToGrab.GetComponent<Rigidbody>();

        if (rbToGrab == null)
        {
            return; // If the object doesn't have a Rigidbody, we can't grab it
        }
        
        // Create and configure the ConfigurableJoint
        ConfigurableJoint grabJoint = gameObjectToGrab.AddComponent<ConfigurableJoint>();
        grabJoint.connectedBody = handJoint.GetComponent<Rigidbody>();

        // Disable auto configuration of the connected anchor to manually set it
        grabJoint.autoConfigureConnectedAnchor = false;

        // Use spring settings for a more flexible grab
        JointDrive springDrive = new JointDrive
        {
            positionSpring = 4000f, // Adjust spring force as needed
            positionDamper = 100f, // Adjust damper for smoothness
            maximumForce = Mathf.Infinity
        };

        grabJoint.xDrive = springDrive;
        grabJoint.yDrive = springDrive;
        grabJoint.zDrive = springDrive;

        // Optionally configure angular drives for spring-like rotation behavior
        JointDrive angularDrive = new JointDrive
        {
            positionSpring = 500f, // Adjust angular spring force as needed
            positionDamper = 50f, // Adjust angular damper for smoothness
            maximumForce = Mathf.Infinity
        };

        grabJoint.angularXDrive = angularDrive;
        grabJoint.angularYZDrive = angularDrive;

        // Set the anchor on the grabbed object to the local grab point
        grabJoint.anchor = anchorHandPalm;
        
        // Set the connected anchor to the local position of the hand palm relative to the hand joint
        grabJoint.connectedAnchor = connectedAnchorHandPalm;

        // Set the target position for the joint to move towards
        grabJoint.targetPosition = Vector3.zero; // Since we're setting the anchors, the target is the anchor itself

        // Add the newly created joint to the list of grabbed joints
        _grabbedJoints.Add(grabJoint);
        _grabbedObject = gameObjectToGrab;
    }

    public void RemoveGrab()
    {
        for (int i = 0; i < _grabbedJoints.Count; i++)
        {
            Destroy(_grabbedJoints[i]);
        }
        
        _grabbedJoints.Clear();
        _grabbedObject = null;
    }
}
