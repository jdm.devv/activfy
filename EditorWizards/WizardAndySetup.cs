﻿#if UNITY_EDITOR

using System.Collections.Generic;
using Activfy.Core;
using UnityEditor;
using UnityEngine;

namespace activfy.EditorWizards
{
    public class WizardAndySetup : ScriptableWizard
    {
        public Animator HumanoidAnimator;

        public AndyRagdollData RagdollData;

        //[Button(nameof(AttemptAutoSetup))] public bool buttonField;

        [ReadOnly] [TextArea] public string Info = "Either manually connect the joints above or you can use the \"Attempt Auto Setup\" button if you have the hips in";

        //[MenuItem("Andy/Configure New Active Ragdoll")]
        static void CreateWizard()
        {
            DisplayWizard<WizardAndySetup>("Active Ragdoll Configuration", "Complete");
            
        }

        void OnWizardCreate()
        {
            RigUp();
            Debug.Log("Andy setup complete!");
        }

        private void OnWizardUpdate()
        {
            isValid = false;

            if (RagdollData != null)
            {
                isValid = RagdollData.IsComplete();
                if (!isValid)
                {
                    errorString = "Ragdoll setup not finished";
                    return;
                }
            }
            
            if (HumanoidAnimator != null)
            {
                isValid = true;
            }
            else
            {
                errorString = "Humanoid Animator not linked";
                
                return;
            }

            errorString = "";
        }

        void OnWizardOtherButton()
        {
            
        }

        public void AttemptAutoSetup()
        {
            _spineList.Clear();

            if (RagdollData.Hips)
            {
                AutoSetupDig(RagdollData.Hips);

                RagdollData.MiddleSpine = _spineList[(int)(_spineList.Count / 2f)];

                Debug.Log("Complete, please review that all of the following connection are correct before continuing");
            }
            else
            {
                Debug.LogWarning("Please set the correct Hips before trying to auto complete the setup");
            }
        }

        private List<Transform> _spineList = new List<Transform>();

        private void AutoSetupDig(Transform part)
        {
            if (part.childCount == 0) return;

            for (int i = 0; i < part.childCount; i++)
            {
                Transform cPart = part.GetChild(i);
                string nameOfPart = cPart.name.ToLower();

                if (nameOfPart.Contains("shoulder")) continue;
                
                if (nameOfPart.Contains("left"))
                {
                    if (!RagdollData.LeftHip)
                        RagdollData.LeftHip = cPart;
                    else if (!RagdollData.LeftKnee)
                        RagdollData.LeftKnee = cPart;
                    else if (!RagdollData.LeftFoot)
                    {
                        RagdollData.LeftFoot = cPart;
                        return;
                    }
                    else if (!RagdollData.LeftArm)
                        RagdollData.LeftArm = cPart;
                    else if (!RagdollData.LeftElbow)
                        RagdollData.LeftElbow = cPart;
                    else if (!RagdollData.LeftHand)
                        RagdollData.LeftHand = cPart;
                }
                else if (nameOfPart.Contains("right"))
                {
                    if (!RagdollData.RightHip)
                        RagdollData.RightHip = cPart;
                    else if (!RagdollData.RightKnee)
                        RagdollData.RightKnee = cPart;
                    else if (!RagdollData.RightFoot)
                    {
                        RagdollData.RightFoot = cPart;
                        return;
                    }
                    else if (!RagdollData.RightArm)
                        RagdollData.RightArm = cPart;
                    else if (!RagdollData.RightElbow)
                        RagdollData.RightElbow = cPart;
                    else if (!RagdollData.RightHand) 
                        RagdollData.RightHand = cPart;
                }
                else if (nameOfPart.Contains("head"))
                {
                    if (!RagdollData.Head)
                        RagdollData.Head = cPart;
                }
                else if (nameOfPart.Contains("spine"))
                {
                    _spineList.Add(cPart);
                }
            }

            for (int i = 0; i < part.childCount; i++)
            {
                AutoSetupDig(part.GetChild(i));
            }
        }

        private void RigUp()
        {
            GameObject activeRagdollRoot = new GameObject($"{HumanoidAnimator.gameObject.name}(Woody)");
            HumanoidAnimator.transform.SetParent(activeRagdollRoot.transform);
            HumanoidAnimator.gameObject.name = "Ragdoll";
            GameObject ragdollRoot = HumanoidAnimator.gameObject;

            GameObject animatedCopy = Instantiate(HumanoidAnimator.gameObject, activeRagdollRoot.transform, true);
            animatedCopy.name = "Animated";

            HumanoidAnimator = animatedCopy.gameObject.GetComponent<Animator>();

            SetupJoint(RagdollData.Hips, null, true);
            
            SetupJoint(RagdollData.LeftHip, RagdollData.Hips, false, -20, 70, 50);
            SetupJoint(RagdollData.RightHip, RagdollData.Hips, false, -20, 70, 50);
            SetupJoint(RagdollData.MiddleSpine, RagdollData.Hips, false, -20, 20, 10, 35);
            
            SetupJoint(RagdollData.LeftKnee, RagdollData.LeftHip, false, -140, 0, 10);
            SetupJoint(RagdollData.LeftFoot, RagdollData.LeftKnee, false, -14, 40, 10);
            
            SetupJoint(RagdollData.RightKnee, RagdollData.RightHip, false, -140, 0, 10);
            SetupJoint(RagdollData.RightFoot, RagdollData.RightKnee, false, -14, 40, 10);
            
            SetupJoint(RagdollData.Head, RagdollData.MiddleSpine, false, -40, 25, 25);
            SetupJoint(RagdollData.LeftArm, RagdollData.MiddleSpine, false, -85, 77, 110);
            SetupJoint(RagdollData.RightArm, RagdollData.MiddleSpine, false, -85, 77, 110);
            
            SetupJoint(RagdollData.LeftElbow, RagdollData.LeftArm, false, -90, 0, 85);
            SetupJoint(RagdollData.LeftHand, RagdollData.LeftElbow, false, -15, 85, 20);
            
            SetupJoint(RagdollData.RightElbow, RagdollData.RightArm, false, -90, 0, 85);
            SetupJoint(RagdollData.RightHand, RagdollData.RightElbow, false, -15, 85, 20);

            ActiveRagdoll activeRagdoll = activeRagdollRoot.AddComponent<ActiveRagdoll>();
            activeRagdoll.AnimatedRoot = animatedCopy.transform;
            activeRagdoll.RagdollRoot = ragdollRoot.transform;
            activeRagdoll.Setup();
        }

        private void SetupJoint(Transform main,
                                Transform parent,
                                bool isHips = false,
                                float lowAngularXLimit = 0.0f,
                                float highAngularXLimit = 0.0f,
                                float angularYLimit = 0.0f,
                                float angularZLimit = 0.0f)
        {
            Rigidbody rigidbody = main.gameObject.AddComponent<Rigidbody>();
            CapsuleCollider capsuleCollider = main.gameObject.AddComponent<CapsuleCollider>();
            ConfigurableJoint configurableJoint = main.gameObject.AddComponent<ConfigurableJoint>();

            rigidbody.mass = 7;

            if (parent)
            {
                configurableJoint.connectedBody = parent.GetComponent<Rigidbody>();
            }

            capsuleCollider.height = 0.002f;
            capsuleCollider.radius = 0.002f;

            rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;

            if (!isHips)
            {
                configurableJoint.xMotion = ConfigurableJointMotion.Locked;
                configurableJoint.yMotion = ConfigurableJointMotion.Locked;
                configurableJoint.zMotion = ConfigurableJointMotion.Locked;

                configurableJoint.angularXMotion = ConfigurableJointMotion.Limited;
                configurableJoint.angularYMotion = ConfigurableJointMotion.Limited;
                configurableJoint.angularZMotion = ConfigurableJointMotion.Limited;

                configurableJoint.lowAngularXLimit =  new SoftJointLimit { limit = lowAngularXLimit };
                configurableJoint.highAngularXLimit = new SoftJointLimit { limit = highAngularXLimit };
                
                configurableJoint.angularYLimit = new SoftJointLimit { limit = angularYLimit };
                configurableJoint.angularZLimit = new SoftJointLimit { limit = angularZLimit };
                
                configurableJoint.axis = Vector3.forward;
                configurableJoint.secondaryAxis = Vector3.right;
 
            }
            
            configurableJoint.projectionMode = JointProjectionMode.PositionAndRotation;
            configurableJoint.enablePreprocessing = true;
        }
    }
}

#endif