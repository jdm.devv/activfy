﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Linq;
using Activfy.Core;
using Activfy.Helpers;
using UnityEditor;
using UnityEngine;

namespace activfy.EditorWizards
{
    public class WizardActivfyRagdoll : ScriptableWizard
    {
        [Header("Activfy Setup")]
        public Transform CharacterRoot;
        public float TotalMass = 70;
        public bool OptimizeAnimated = true;

        [Header("Bones")]
        public Transform Pelvis;
        public Transform LeftHips;
        public Transform LeftKnee;
        public Transform RightHips;
        public Transform RightKnee;
        public Transform LeftArm;
        public Transform LeftElbow;
        public Transform RightArm;
        public Transform RightElbow;
        public Transform MiddleSpine;
        public Transform Head;
        
        [Header("Main Hips")]
        public VectorDirections PelvisUp;
        public VectorDirections PelvisForward;

        [Header("Movement Axis")]
        public VectorDirections LeftHipsMainAxis;
        public VectorDirections LeftKneeMainAxis;
        public VectorDirections RightHipsMainAxis;
        public VectorDirections RightKneeMainAxis;
        public VectorDirections LeftArmMainAxis;
        public VectorDirections LeftElbowMainAxis;
        public VectorDirections RightArmMainAxis;
        public VectorDirections RightElbowMainAxis;
        public VectorDirections MiddleSpineMainAxis;
        public VectorDirections HeadMainAxis;
        
        [Header("Secondary Movement Axis")]
        public VectorDirections LeftHipsSecondaryAxis;
        public VectorDirections LeftKneeSecondaryAxis;
        public VectorDirections RightHipsSecondaryAxis;
        public VectorDirections RightKneeSecondaryAxis;
        public VectorDirections LeftArmSecondaryAxis;
        public VectorDirections LeftElbowSecondaryAxis;
        public VectorDirections RightArmSecondaryAxis;
        public VectorDirections RightElbowSecondaryAxis;
        public VectorDirections MiddleSpineSecondaryAxis;
        public VectorDirections HeadSecondaryAxis;
        
        [Header("Settings")]
        private Vector3 _right = Vector3.right;
        private Vector3 _up = Vector3.up;
        private Vector3 _forward = Vector3.forward;

        private Vector3 _worldRight = Vector3.right;
        private Vector3 _worldUp = Vector3.up;
        private Vector3 _worldForward = Vector3.forward;

        private ArrayList _bones;
        private BoneInfo _rootBone;

        private ActiveRagdoll _activeRagdoll;

        class BoneInfo
        {
            public string Name;
            public Transform Anchor;
            public ConfigurableJoint Joint;
            public BoneInfo Parent;
            public float MinLimit;
            public float MaxLimit;
            public float SwingLimit;
            public float ZLimit;
            public Vector3 Axis;
            public Vector3 NormalAxis;
            public float RadiusScale;
            public Type ColliderType;
            public ArrayList Children = new ArrayList();
            public float Density;
            public float SummedMass;
        }

        protected override bool DrawWizardGUI()
        {
            bool value = base.DrawWizardGUI();

            GUIStyle style = new GUIStyle(GUI.skin.button);
            style.richText = true;

            if (Pelvis &&
                LeftHips &&
                RightHips &&
                LeftKnee &&
                RightKnee &&
                LeftArm &&
                RightArm &&
                LeftElbow &&
                RightElbow &&
                MiddleSpine &&
                Head)
            {
                EditorGUILayout.HelpBox("After setting up your bones, you must configure their movement axis. You can either assign them manually, or use the auto setup option. However, for the auto setup to work properly, your model must be in a correct T-Pose and facing the world's forward axis.", MessageType.Info);

                if (GUILayout.Button("<b>Attempt Auto Setup</b>\n" +
                                     "<i>Required to be in a T-Pose</i>", style))
                {
                    AutoSetupDirections();
                }
            }

            return value;
        }

        string CheckConsistency()
        {
            PrepareBones();
            Hashtable map = new Hashtable();
            foreach (BoneInfo bone in _bones)
            {
                if (bone.Anchor)
                {
                    if (map[bone.Anchor] != null)
                    {
                        BoneInfo oldBone = (BoneInfo)map[bone.Anchor];
                        return $"Both '{bone.Name}' and '{oldBone.Name}' cannot be assigned to the same bone.";
                    }
                    map[bone.Anchor] = bone;
                }
            }

            foreach (BoneInfo bone in _bones)
            {
                if (bone.Anchor == null)
                    return $"The bone '{bone.Name}' has not been assigned yet.\n";
            }

            return "";
        }

        void OnDrawGizmos()
        {
            if (Pelvis)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawRay(Pelvis.position, Pelvis.TransformDirection(_right));
                Gizmos.color = Color.green;
                Gizmos.DrawRay(Pelvis.position, Pelvis.TransformDirection(_up));
                Gizmos.color = Color.blue;
                Gizmos.DrawRay(Pelvis.position, Pelvis.TransformDirection(_forward));
            }
        }

        [MenuItem("Tools/Activfy/Create Active Ragdoll...", false, 1700)]
        static void CreateWizard()
        {
            ScriptableWizard.DisplayWizard<WizardActivfyRagdoll>("[Activfy] Create Active Ragdoll", "Create");
        }

        void DecomposeVector(out Vector3 normalCompo, out Vector3 tangentCompo, Vector3 outwardDir, Vector3 outwardNormal)
        {
            outwardNormal = outwardNormal.normalized;
            normalCompo = outwardNormal * Vector3.Dot(outwardDir, outwardNormal);
            tangentCompo = outwardDir - normalCompo;
        }

        void CalculateAxes()
        {
            if (Head != null && Pelvis != null)
                _up = CalculateDirectionAxis(Pelvis.InverseTransformPoint(Head.position));
            if (RightElbow != null && Pelvis != null)
            {
                DecomposeVector(out _, out var removed, Pelvis.InverseTransformPoint(RightElbow.position), _up);
                _right = CalculateDirectionAxis(removed);
            }

            _forward = Vector3.Cross(_right, _up);
            // if (FlipForward)
            //     _forward = -_forward;
        }

        void OnWizardUpdate()
        {
            errorString = CheckConsistency();
            CalculateAxes();

            if (errorString.Length != 0)
            {
                helpString = "To properly position your character, make sure it is in T-Stand and facing towards the positive Z axis (i.e., the \"forward\" vector) of the world. Then, proceed to drag each bone from the hierarchy into its designated slot.";
            }
            else
            {
                helpString = "";
            }

            isValid = errorString.Length == 0;

            if (LeftHips && !LeftKnee)
                LeftKnee = LeftHips.GetChild(0);
            
            if (RightHips && !RightKnee)
                RightKnee = RightHips.GetChild(0);
            
            if (LeftArm && !LeftElbow)
                LeftElbow = LeftArm.GetChild(0);
            
            if (RightArm && !RightElbow)
                RightElbow = RightArm.GetChild(0);
        }
        
        private void AutoSetupDirections()
        {
            // Main Axis
            PelvisUp = ActivfiUtil.VectorDirectionsFromVector(Vector3Int.RoundToInt(Pelvis.InverseTransformDirection(Vector3.up)));
            PelvisForward = ActivfiUtil.VectorDirectionsFromVector(Vector3Int.RoundToInt(Pelvis.InverseTransformDirection(Vector3.forward)));
            
            SetupDirections(ref HeadMainAxis, ref HeadSecondaryAxis, Head, Vector3.left, Vector3.forward);
            SetupDirections(ref MiddleSpineMainAxis, ref MiddleSpineSecondaryAxis, MiddleSpine, Vector3.left, Vector3.forward);

            SetupDirections(ref LeftArmMainAxis, ref LeftArmSecondaryAxis, LeftArm, Vector3.up);
            SetupDirections(ref RightArmMainAxis, ref RightArmSecondaryAxis, RightArm, Vector3.up);

            SetupDirections(ref LeftElbowMainAxis, ref LeftElbowSecondaryAxis, LeftElbow, Vector3.up);
            SetupDirections(ref RightElbowMainAxis, ref RightElbowSecondaryAxis, RightElbow, Vector3.up);

            SetupDirections(ref LeftHipsMainAxis, ref LeftHipsSecondaryAxis, LeftHips, Vector3.right);
            SetupDirections(ref RightHipsMainAxis, ref RightHipsSecondaryAxis, RightHips, Vector3.right);

            SetupDirections(ref LeftKneeMainAxis, ref LeftKneeSecondaryAxis, LeftKnee, Vector3.right);
            SetupDirections(ref RightKneeMainAxis, ref RightKneeSecondaryAxis, RightKnee, Vector3.right);

            OnWizardUpdate();
        }

        private void SetupDirections(ref VectorDirections mainAxis,
                                     ref VectorDirections secondaryAxis,
                                     Transform bodyPart,
                                     Vector3 worldMainAxis,
                                     Vector3 secondaryWorldDirection = default)
        {
            mainAxis = ActivfiUtil.VectorDirectionsFromVector(Vector3Int.RoundToInt(bodyPart.InverseTransformDirection(worldMainAxis)));

            if (secondaryWorldDirection == Vector3.zero)
            {
                if (bodyPart.childCount > 0)
                {
                    Transform child = bodyPart.GetChild(0);
                    secondaryWorldDirection = (child.position - bodyPart.position).normalized;
                }
                else
                {
                    if (bodyPart.parent)
                    {
                        secondaryWorldDirection = (bodyPart.position - bodyPart.parent.position).normalized;
                    }
                }
            }

            if (secondaryWorldDirection != Vector3.zero)
            {
                secondaryAxis = ActivfiUtil.VectorDirectionsFromVector(Vector3Int.RoundToInt(bodyPart.InverseTransformDirection(secondaryWorldDirection)));
            }
        }

        void PrepareBones()
        {
            if (Pelvis)
            {
                _worldRight = Pelvis.TransformDirection(_right);
                _worldUp = Pelvis.TransformDirection(_up);
                _worldForward = Pelvis.TransformDirection(_forward);
            }

            _bones = new ArrayList();

            _rootBone = new BoneInfo
            {
                Name = "Pelvis",
                Anchor = Pelvis,
                Parent = null,
                Density = 2.5F
            };
            _bones.Add(_rootBone);
            
            AddJoint("Left Hip", LeftHips, "Pelvis", ActivfiUtil.VectorFromVectorDirections(LeftHipsMainAxis), ActivfiUtil.VectorFromVectorDirections(LeftHipsSecondaryAxis), -31, 117, 50, typeof(CapsuleCollider), 0.3F, 1.5F, 60.0f);
            AddJoint("Right Hip", RightHips, "Pelvis", ActivfiUtil.VectorFromVectorDirections(RightHipsMainAxis), ActivfiUtil.VectorFromVectorDirections(RightHipsSecondaryAxis), -31, 117, 50, typeof(CapsuleCollider), 0.3F, 1.5F, 60.0f);

            AddJoint("Left Knee", LeftKnee, "Left Hip", ActivfiUtil.VectorFromVectorDirections(LeftKneeMainAxis), ActivfiUtil.VectorFromVectorDirections(LeftKneeSecondaryAxis), -135, 5, 10, typeof(CapsuleCollider), 0.25F, 1.5F);
            AddJoint("Right Knee", RightKnee, "Right Hip", ActivfiUtil.VectorFromVectorDirections(RightKneeMainAxis), ActivfiUtil.VectorFromVectorDirections(RightKneeSecondaryAxis),-135, 5, 10, typeof(CapsuleCollider), 0.25F, 1.5F);

            AddJoint("Middle Spine", MiddleSpine, "Pelvis", ActivfiUtil.VectorFromVectorDirections(MiddleSpineMainAxis), ActivfiUtil.VectorFromVectorDirections(MiddleSpineSecondaryAxis), -20, 20, 10, null, 1, 2.5F, 90);
            
            AddJoint("Left Arm", LeftArm, "Middle Spine", ActivfiUtil.VectorFromVectorDirections(LeftArmMainAxis), ActivfiUtil.VectorFromVectorDirections(LeftArmSecondaryAxis), -130, 85, 70, typeof(CapsuleCollider), 0.25F, 1.0F, 75.0f);
            AddJoint("Right Arm", RightArm, "Middle Spine", -ActivfiUtil.VectorFromVectorDirections(RightArmMainAxis), ActivfiUtil.VectorFromVectorDirections(RightArmSecondaryAxis), -130, 85, 70, typeof(CapsuleCollider), 0.25F, 1.0F, 75.0f);
            
            AddJoint("Left Elbow", LeftElbow, "Left Arm", ActivfiUtil.VectorFromVectorDirections(LeftElbowMainAxis), ActivfiUtil.VectorFromVectorDirections(LeftElbowSecondaryAxis), -105, 15, 85, typeof(CapsuleCollider), 0.20F, 1.0F);
            AddJoint("Right Elbow", RightElbow, "Right Arm", -ActivfiUtil.VectorFromVectorDirections(RightElbowMainAxis), ActivfiUtil.VectorFromVectorDirections(RightElbowSecondaryAxis), -105, 15, 85, typeof(CapsuleCollider), 0.20F, 1.0F);
            
            AddJoint("Head", Head, "Middle Spine", ActivfiUtil.VectorFromVectorDirections(HeadMainAxis), ActivfiUtil.VectorFromVectorDirections(HeadSecondaryAxis), -40, 25, 25, null, 1, 1.0F, 50.0f);
            
            // Old Code
            // AddMirroredJoint("Hips", LeftHips, RightHips, "Pelvis", _worldRight, _worldForward, -20, 70, 50, typeof(CapsuleCollider), 0.3F, 1.5F);
            // AddMirroredJoint("Knee", LeftKnee, RightKnee, "Hips", _worldRight, _worldForward, -145, -2, 10, typeof(CapsuleCollider), 0.25F, 1.5F);
            //
            // AddJoint("Middle Spine", MiddleSpine, "Pelvis", _worldRight, _worldForward, -20, 20, 10, null, 1, 2.5F);
            //
            // AddMirroredJoint("Arm", LeftArm, RightArm, "Middle Spine", _worldUp, _worldForward, -110, 75, 50, typeof(CapsuleCollider), 0.25F, 1.0F);
            // AddMirroredJoint("Elbow", LeftElbow, RightElbow, "Arm", _worldUp, _worldForward, 0, 90, 85, typeof(CapsuleCollider), 0.20F, 1.0F);
            //
            // AddJoint("Head", Head, "Middle Spine", _worldUp, _worldForward, -40, 25, 25, null, 1, 1.0F);
        }

        private Vector3 GetNormalAxis(VectorDirections mainAxis, Vector3 worldNormal)
        {
            if (mainAxis == VectorDirections.Back ||
                mainAxis == VectorDirections.Left ||
                mainAxis == VectorDirections.Down)
                return -worldNormal;
            return worldNormal;
        }

        void OnWizardCreate()
        {
            Cleanup();
            SetupActivfyStructure();
            BuildCapsules();
            AddBreastColliders();
            AddHeadCollider();

            BuildBodies();
            BuildJoints();
            CalculateMass();

            ActivfyFinalize();
        }

        private void SetupActivfyStructure()
        {
            // Create Active Ragdoll Root
            GameObject activeRagdollRoot = new($"{CharacterRoot.gameObject.name}(Active Ragdoll)");
            activeRagdollRoot.transform.position = CharacterRoot.transform.position;

            CharacterRoot.SetParent(activeRagdollRoot.transform);
            CharacterRoot.gameObject.name = "Ragdoll";
            GameObject ragdollRoot = CharacterRoot.gameObject;

            // Create Animated Copy
            GameObject animatedCopy = Instantiate(CharacterRoot.gameObject, activeRagdollRoot.transform);
            animatedCopy.name = "Animated";

            Animator animator = animatedCopy.GetComponentInChildren<Animator>();
            if (animator) 
            {
                animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
                animator.updateMode = AnimatorUpdateMode.Normal;
                animator.applyRootMotion = false;
            }

            // Clean up animators
            ragdollRoot.GetComponentsInChildren<Animator>().ToList().ForEach(DestroyImmediate);

            // Set up SimpleActiveRagdoll component
            _activeRagdoll = activeRagdollRoot.AddComponent<ActiveRagdoll>();
            _activeRagdoll.AnimatedRoot = animatedCopy.transform;
            _activeRagdoll.RagdollRoot = ragdollRoot.transform;
        }
        
        private void ActivfyFinalize()
        {
            _activeRagdoll.PelvisUp = PelvisUp;
            _activeRagdoll.Setup();
        }

        BoneInfo FindBone(string name)
        {
            foreach (BoneInfo bone in _bones)
            {
                if (bone.Name == name)
                    return bone;
            }
            return null;
        }

        void AddMirroredJoint(string name, Transform leftAnchor, Transform rightAnchor, string parent, Vector3 worldTwistAxis, Vector3 worldSwingAxis, float minLimit, float maxLimit, float swingLimit, Type colliderType, float radiusScale, float density)
        {
            AddJoint("Left " + name, leftAnchor, parent, worldTwistAxis, worldSwingAxis, minLimit, maxLimit, swingLimit, colliderType, radiusScale, density);
            AddJoint("Right " + name, rightAnchor, parent, worldTwistAxis, worldSwingAxis, minLimit, maxLimit, swingLimit, colliderType, radiusScale, density);
        }

        void AddJoint(string name, Transform anchor, string parent, Vector3 worldTwistAxis, Vector3 worldSwingAxis, float minLimit, float maxLimit, float swingLimit, Type colliderType, float radiusScale, float density, float zLimit = 0.0f)
        {
            BoneInfo bone = new BoneInfo();
            bone.Name = name;
            bone.Anchor = anchor;
            bone.Axis = worldTwistAxis;
            bone.NormalAxis = worldSwingAxis;
            bone.MinLimit = minLimit;
            bone.MaxLimit = maxLimit;
            bone.SwingLimit = swingLimit;
            bone.ZLimit = zLimit;
            bone.Density = density;
            bone.ColliderType = colliderType;
            bone.RadiusScale = radiusScale;

            if (FindBone(parent) != null)
                bone.Parent = FindBone(parent);
            else if (name.StartsWith("Left"))
                bone.Parent = FindBone("Left " + parent);
            else if (name.StartsWith("Right"))
                bone.Parent = FindBone("Right " + parent);


            bone.Parent.Children.Add(bone);
            _bones.Add(bone);
        }

        void BuildCapsules()
        {
            foreach (BoneInfo bone in _bones)
            {
                if (bone.ColliderType != typeof(CapsuleCollider))
                    continue;

                int direction;
                float distance;
                if (bone.Children.Count == 1)
                {
                    BoneInfo childBone = (BoneInfo)bone.Children[0];
                    Vector3 endPoint = childBone.Anchor.position;
                    CalculateDirection(bone.Anchor.InverseTransformPoint(endPoint), out direction, out distance);
                }
                else
                {
                    Vector3 endPoint = (bone.Anchor.position - bone.Parent.Anchor.position) + bone.Anchor.position;
                    CalculateDirection(bone.Anchor.InverseTransformPoint(endPoint), out direction, out distance);

                    if (bone.Anchor.GetComponentsInChildren(typeof(Transform)).Length > 1)
                    {
                        Bounds bounds = new Bounds();
                        foreach (Transform child in bone.Anchor.GetComponentsInChildren(typeof(Transform)))
                        {
                            bounds.Encapsulate(bone.Anchor.InverseTransformPoint(child.position));
                        }

                        distance = distance > 0 ? bounds.max[direction] : bounds.min[direction];
                    }
                }

                CapsuleCollider collider = Undo.AddComponent<CapsuleCollider>(bone.Anchor.gameObject);
                collider.direction = direction;

                Vector3 center = Vector3.zero;
                center[direction] = distance * 0.5F;
                collider.center = center;
                collider.height = Mathf.Abs(distance);
                collider.radius = Mathf.Abs(distance * bone.RadiusScale);
                
                bone.Anchor.gameObject.layer = LayerMask.NameToLayer("ActiveRagdoll");
            }
        }

        void Cleanup()
        {
            foreach (BoneInfo bone in _bones)
            {
                if (!bone.Anchor)
                    continue;

                Component[] joints = bone.Anchor.GetComponentsInChildren(typeof(Joint));
                foreach (Joint joint in joints)
                    Undo.DestroyObjectImmediate(joint);

                Component[] bodies = bone.Anchor.GetComponentsInChildren(typeof(Rigidbody));
                foreach (Rigidbody body in bodies)
                    Undo.DestroyObjectImmediate(body);

                Component[] colliders = bone.Anchor.GetComponentsInChildren(typeof(Collider));
                foreach (Collider collider in colliders)
                    Undo.DestroyObjectImmediate(collider);
            }
        }

        void BuildBodies()
        {
            foreach (BoneInfo bone in _bones)
            {
                Undo.AddComponent<Rigidbody>(bone.Anchor.gameObject);
                    
                Rigidbody rigidbody = bone.Anchor.GetComponent<Rigidbody>();
                    
                rigidbody.mass = bone.Density;
                rigidbody.drag = 0.1f; // TODO add to part of bone struct
                rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
            }
        }

        void BuildJoints()
        {
            foreach (BoneInfo bone in _bones)
            {
                ConfigurableJoint joint = Undo.AddComponent<ConfigurableJoint>(bone.Anchor.gameObject);
                bone.Joint = joint;
                
                joint.enablePreprocessing = false; // turn off to handle degenerated scenarios, like spawning inside geometry.
                joint.projectionMode = JointProjectionMode.PositionAndRotation;

                if (bone.Parent != null)
                {
                    // Vector3 secondaryAxis = CalculateDirectionAxis(bone.Anchor.InverseTransformDirection(bone.NormalAxis)); // Old way
                    Vector3 secondaryAxis = bone.NormalAxis;
                    
                    // Setup connection and axis
                    joint.axis = bone.Axis;
                    joint.secondaryAxis = secondaryAxis;
                    joint.anchor = Vector3.zero;
                    joint.connectedBody = bone.Parent.Anchor.GetComponent<Rigidbody>();

                    // Setup limits
                    SoftJointLimit limit = new SoftJointLimit();
                    limit.contactDistance = 0; // default to zero, which automatically sets contact distance.

                    limit.limit = bone.MinLimit;
                    joint.lowAngularXLimit = limit;

                    limit.limit = bone.MaxLimit;
                    joint.highAngularXLimit = limit;
                    
                    // re set in case the lowAngularXLimit is a positive value
                    limit.limit = bone.MinLimit;
                    joint.lowAngularXLimit = limit;

                    limit.limit = bone.SwingLimit;
                    joint.angularYLimit = limit;

                    limit.limit = bone.ZLimit;
                    joint.angularZLimit = limit;

                    joint.xMotion = ConfigurableJointMotion.Locked;
                    joint.yMotion = ConfigurableJointMotion.Locked;
                    joint.zMotion = ConfigurableJointMotion.Locked;

                    joint.angularXMotion = ConfigurableJointMotion.Limited;
                    joint.angularYMotion = ConfigurableJointMotion.Limited;
                    joint.angularZMotion = ConfigurableJointMotion.Limited;
                }

                joint.rotationDriveMode = RotationDriveMode.Slerp;
            }
        }

        void CalculateMassRecurse(BoneInfo bone)
        {
            float mass = bone.Anchor.GetComponent<Rigidbody>().mass;
            foreach (BoneInfo child in bone.Children)
            {
                CalculateMassRecurse(child);
                mass += child.SummedMass;
            }
            bone.SummedMass = mass;
        }

        void CalculateMass()
        {
            // Calculate allChildMass by summing all bodies
            CalculateMassRecurse(_rootBone);

            // Rescale the mass so that the whole character weights totalMass
            float massScale = TotalMass / _rootBone.SummedMass;
            foreach (BoneInfo bone in _bones)
                bone.Anchor.GetComponent<Rigidbody>().mass *= massScale;

            // Recalculate allChildMass by summing all bodies
            CalculateMassRecurse(_rootBone);
        }

        static void CalculateDirection(Vector3 point, out int direction, out float distance)
        {
            // Calculate longest axis
            direction = 0;
            if (Mathf.Abs(point[1]) > Mathf.Abs(point[0]))
                direction = 1;
            if (Mathf.Abs(point[2]) > Mathf.Abs(point[direction]))
                direction = 2;

            distance = point[direction];
        }

        static Vector3 CalculateDirectionAxis(Vector3 point)
        {
            CalculateDirection(point, out var direction, out var distance);
            Vector3 axis = Vector3.zero;
            if (distance > 0)
                axis[direction] = 1.0F;
            else
                axis[direction] = -1.0F;
            return axis;
        }

        static int SmallestComponent(Vector3 point)
        {
            int direction = 0;
            if (Mathf.Abs(point[1]) < Mathf.Abs(point[0]))
                direction = 1;
            if (Mathf.Abs(point[2]) < Mathf.Abs(point[direction]))
                direction = 2;
            return direction;
        }

        static int LargestComponent(Vector3 point)
        {
            int direction = 0;
            if (Mathf.Abs(point[1]) > Mathf.Abs(point[0]))
                direction = 1;
            if (Mathf.Abs(point[2]) > Mathf.Abs(point[direction]))
                direction = 2;
            return direction;
        }

        static int SecondLargestComponent(Vector3 point)
        {
            int smallest = SmallestComponent(point);
            int largest = LargestComponent(point);
            if (smallest < largest)
                (largest, smallest) = (smallest, largest);

            return smallest switch
            {
                0 when largest == 1 => 2,
                0 when largest == 2 => 1,
                _ => 0
            };
        }

        Bounds Clip(Bounds bounds, Transform relativeTo, Transform clipTransform, bool below)
        {
            int axis = LargestComponent(bounds.size);

            if (Vector3.Dot(_worldUp, relativeTo.TransformPoint(bounds.max)) > Vector3.Dot(_worldUp, relativeTo.TransformPoint(bounds.min)) == below)
            {
                Vector3 min = bounds.min;
                min[axis] = relativeTo.InverseTransformPoint(clipTransform.position)[axis];
                bounds.min = min;
            }
            else
            {
                Vector3 max = bounds.max;
                max[axis] = relativeTo.InverseTransformPoint(clipTransform.position)[axis];
                bounds.max = max;
            }
            return bounds;
        }

        Bounds GetBreastBounds(Transform relativeTo)
        {
            // Pelvis bounds
            Bounds bounds = new Bounds();
            bounds.Encapsulate(relativeTo.InverseTransformPoint(LeftHips.position));
            bounds.Encapsulate(relativeTo.InverseTransformPoint(RightHips.position));
            bounds.Encapsulate(relativeTo.InverseTransformPoint(LeftArm.position));
            bounds.Encapsulate(relativeTo.InverseTransformPoint(RightArm.position));
            Vector3 size = bounds.size;
            size[SmallestComponent(bounds.size)] = size[LargestComponent(bounds.size)] / 2.0F;
            bounds.size = size;
            return bounds;
        }

        void AddBreastColliders()
        {
            // Middle spine and pelvis
            if (MiddleSpine != null && Pelvis != null)
            {
                Bounds bounds;
                BoxCollider box;

                // Middle spine bounds
                bounds = Clip(GetBreastBounds(Pelvis), Pelvis, MiddleSpine, false);
                box = Undo.AddComponent<BoxCollider>(Pelvis.gameObject);
                box.center = bounds.center;
                box.size = bounds.size;

                bounds = Clip(GetBreastBounds(MiddleSpine), MiddleSpine, MiddleSpine, true);
                box = Undo.AddComponent<BoxCollider>(MiddleSpine.gameObject);
                box.center = bounds.center;
                box.size = bounds.size;
            }
            // Only pelvis
            else
            {
                Bounds bounds = new Bounds();
                bounds.Encapsulate(Pelvis.InverseTransformPoint(LeftHips.position));
                bounds.Encapsulate(Pelvis.InverseTransformPoint(RightHips.position));
                bounds.Encapsulate(Pelvis.InverseTransformPoint(LeftArm.position));
                bounds.Encapsulate(Pelvis.InverseTransformPoint(RightArm.position));

                Vector3 size = bounds.size;
                size[SmallestComponent(bounds.size)] = size[LargestComponent(bounds.size)] / 2.0F;

                BoxCollider box = Undo.AddComponent<BoxCollider>(Pelvis.gameObject);
                box.center = bounds.center;
                box.size = size;
            }
        }

        void AddHeadCollider()
        {
            if (Head.GetComponent<Collider>())
                Destroy(Head.GetComponent<Collider>());

            float radius = Vector3.Distance(LeftArm.transform.position, RightArm.transform.position) / 4;

            SphereCollider sphere = Undo.AddComponent<SphereCollider>(Head.gameObject);
            sphere.radius = radius;
            Vector3 center = Vector3.zero;

            CalculateDirection(Head.InverseTransformPoint(Pelvis.position), out var direction, out var distance);
            if (distance > 0)
                center[direction] = -radius;
            else
                center[direction] = radius;
            sphere.center = center;
        }
    }
}

#endif