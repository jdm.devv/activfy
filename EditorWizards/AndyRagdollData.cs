﻿using System;
using UnityEngine;

namespace activfy.EditorWizards
{
    [Serializable]
    public class AndyRagdollData
    {
        [Header("Body")] public Transform Hips;
        public Transform MiddleSpine;
        public Transform Head;
        [Header("Legs")] public Transform LeftHip;
        public Transform LeftKnee;
        public Transform LeftFoot;
        public Transform RightHip;
        public Transform RightKnee;
        public Transform RightFoot;
        [Header("Arms")] public Transform LeftArm;
        public Transform LeftElbow;
        public Transform LeftHand;
        public Transform RightArm;
        public Transform RightElbow;
        public Transform RightHand;

        public bool IsComplete()
        {
            return Hips &&
                   MiddleSpine &&
                   Head &&
                   LeftHip &&
                   LeftKnee &&
                   LeftFoot &&
                   RightHip &&
                   RightKnee &&
                   RightFoot &&
                   LeftArm &&
                   LeftElbow &&
                   LeftHand &&
                   RightArm &&
                   RightElbow &&
                   RightHand;
        }
    }
}