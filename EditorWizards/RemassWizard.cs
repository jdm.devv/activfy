﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using Activfy.Core;
using UnityEditor;
using UnityEngine;

namespace activfy.EditorWizards
{
    public class RemassWizard : ScriptableWizard
    {
        [Serializable]
        class BoneInfo
        {
            [ReadOnly] public string BoneName;
            [HideInInspector] public JointData Data;
            [ReadOnly] public float CurrentMass;
            [Tooltip("The mass for this bone")]
            [Min(0.0f)] public float NewMass = 0;
            [ReadOnly] public float NewSpring;
            [ReadOnly] public float NewDamper;
            
            [HideInInspector] public float CurrentSpring;
            [HideInInspector] public float CurrentDamper;
            [Tooltip("The percentage of the total body mass that is made up of this bone.")]
            [ReadOnly] public string MassPercent;
        }

        [Tooltip("The current total body mass")]
        [SerializeField, ReadOnly] private float _currentMass;
        [Tooltip("The new total body mass")]
        [SerializeField, Min(0.0f)] private float _newMass;
        [Tooltip("Should Spring and Damper also scale with the new masses?")]
        [SerializeField] private bool _recalculateJoints = true;
        [SerializeReference] private List<BoneInfo> _bonesInfo = new List<BoneInfo>();

        public static void CreateWizard(ActiveRagdoll activeRagdoll)
        {
            RemassWizard wizard = DisplayWizard<RemassWizard>("Remass Active Ragdoll", "Apply", "Reset");

            wizard.OnLoad(activeRagdoll);
        }

        public void OnLoad(ActiveRagdoll activeRagdoll)
        {
            _bonesInfo.Clear();

            float totalMass = 0;

            for (int i = 0; i < activeRagdoll.AllJointData.Count; i++)
            {
                JointData jointData = activeRagdoll.AllJointData[i];

                float jointMass = jointData.Rigid.mass;

                _bonesInfo.Add(new BoneInfo()
                {
                    BoneName = jointData.Rigid.gameObject.name,
                    Data = jointData,
                    CurrentMass = jointMass,
                    NewMass = jointMass,
                    CurrentDamper = jointData.Damper,
                    NewDamper = jointData.Damper,
                    CurrentSpring = jointData.Spring,
                    NewSpring = jointData.Spring
                });

                totalMass += jointMass;
            }

            _newMass = _currentMass = totalMass;
            
            Remass();
        }

        protected override bool DrawWizardGUI()
        {
            bool value = base.DrawWizardGUI();

            GUIStyle style = new GUIStyle(GUI.skin.button);
            style.richText = true;

            // EditorGUILayout.HelpBox(
            //     "If you'd like to distribute the mass proportionally for a normal human please set the new mass above and then the button below. You can also set it manually in the list above.",
            //     MessageType.Info);

            // if (GUILayout.Button("<b>Normal Mass Distribution</b>\n<i>proportions for human</i>", style))
            // {
            // }

            return value;
        }

        private float _previousMass = 0;

        private void OnWizardUpdate()
        {
            if (_previousMass != _newMass)
            {
                _previousMass = _newMass;
            }
            else
            {
                _previousMass = _newMass = GetTotalMass();
            }

            Remass();
        }

        private void Remass()
        {
            float totalNewMass = GetTotalMass();

            if (totalNewMass <= 0 && _newMass > 0)
            {
                float tempMass = _newMass;
                ResetMass();
                totalNewMass = GetTotalMass();
                _newMass = tempMass;
            }
            
            if (totalNewMass <= 0 || _newMass <= 0)
                return;

            foreach (var bone in _bonesInfo)
            {
                float percent = bone.NewMass / totalNewMass;
                float changePercent = 1.0f;
                
                bone.MassPercent = percent.ToString("P");
                bone.NewMass = percent * _newMass;
                
                if (bone.CurrentMass >= 0)
                {
                    changePercent = bone.NewMass / bone.CurrentMass;
                }

                if (_recalculateJoints)
                {
                    bone.NewSpring = changePercent * bone.CurrentSpring;
                    bone.NewDamper = changePercent * bone.CurrentDamper;
                }
                else
                {
                    bone.NewSpring = bone.CurrentSpring;
                    bone.NewDamper = bone.CurrentDamper;
                }
            }
        }

        private float GetTotalMass()
        {
            float totalNewMass = 0;

            foreach (var bone in _bonesInfo)
            {
                totalNewMass += bone.NewMass;
            }

            return totalNewMass;
        }

        private void ResetMass()
        {
            foreach (var bone in _bonesInfo)
            {
                bone.NewMass = bone.CurrentMass;
            }

            _previousMass = _newMass = _currentMass;
        }

        void OnWizardCreate()
        {
            foreach (var bone in _bonesInfo)
            {
                bone.Data.Rigid.mass = bone.NewMass;

                bone.Data.Spring = bone.NewSpring;
                bone.Data.Damper = bone.NewDamper;
            }
        }

        private void OnWizardOtherButton()
        {
            ResetMass();
        }
    }
}

#endif