# Active Ragdoll Tool Project

## Overview

The goal of this project is to create an Active Ragdoll Tool for Unity that allows developers to quickly and easily add active ragdoll physics to their characters. The tool should include configurable joints for simulating the physics of the character's joints, as well as a controller for stabilizing and balancing the character, a movement controller, and an active ragdoll animation controller.

## Steps

1. [ ] **Setup Ragdoll Model** - Create a low-poly mesh that matches the character's proportions, add a separate armature for the ragdoll physics, create colliders for each limb and joint of the character, parent the colliders to the armature bones, and disable the armature animation component. (Due: May 31st)
2. [ ] **Add Configurable Joints** - Add Configurable Joints to each bone in the ragdoll armature, configure joint settings to match the character's range of motion and physical limitations, and adjust the anchor and connected anchor points to match the character's proportions. (Due: June 7th)
3. [ ] **Implement Stabilization and Balance Controller** - Create a script to manage the stabilization and balance of the ragdoll, implement a proportional controller that uses the position and orientation of the ragdoll's limbs to stabilize and balance the character, implement a PID controller to adjust the lifting force needed to lift the limbs into the desired position, take into account the weight of the limbs and the effects of gravity, and handle edge cases where the ragdoll falls outside the desired stabilization range. (Due: June 14th)
4. [ ] **Implement Movement Controller** - Create a script to manage the movement of the ragdoll, implement a controller that allows the character to move and interact with the environment, use physics-based forces to move the ragdoll's limbs and joints, take into account the weight and inertia of the character, and handle edge cases where the ragdoll collides with objects in the environment. (Due: June 21st)
5. [ ] **Implement Active Ragdoll Animation Controller** - Create a script to manage the blending between ragdoll physics and animations, use Unity's Animation system to blend between ragdoll physics and animations, allow for a variety of different animation sets and transitions, and handle edge cases where the ragdoll animations conflict with the physical limitations of the character. (Due: June 28th)
6. [ ] **Test and Optimize** - Test the active ragdoll tool in a variety of different scenarios and environments, optimize the tool to ensure that it performs well on a wide range of hardware configurations, profile the performance of the tool to identify any bottlenecks or performance issues, refine the tool based on feedback and testing, and document the tool and provide example projects and tutorials for other developers to use. (Due: July 5th)

Each of these steps can be further broken down into smaller tasks and sub-tasks as needed, depending on the specific requirements and challenges of the project. The checkboxes can be checked off as each step is completed, and the due dates can be adjusted as needed based on the progress of the project.
