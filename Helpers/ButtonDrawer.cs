﻿using System.Text;

namespace Activfy.Helpers
{
    using UnityEngine;
 
#if UNITY_EDITOR
    using UnityEditor;
#endif
 
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(Button2Attribute))]
    public class Button2Drawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            string methodName = (attribute as Button2Attribute)?.MethodName;
            Object target = property.serializedObject.targetObject;
            System.Type type = target.GetType();
            System.Reflection.MethodInfo method = type.GetMethod(methodName);
            if (method == null)
            {
                GUI.Label(position, "Method could not be found. Is it public?");
                return;
            }
            if (method.GetParameters().Length > 0)
            {
                GUI.Label(position, "Method cannot have parameters.");
                return;
            }
            if (GUI.Button(position, AddSpaceAfterCapital(method.Name)))
            {
                method.Invoke(target, null);
            }
        }
        
        public static string AddSpaceAfterCapital(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            StringBuilder result = new StringBuilder(input.Length * 2);

            result.Append(input[0]);
            for (int i = 1; i < input.Length; i++)
            {
                if (char.IsUpper(input[i]))
                {
                    result.Append(' ');
                }
                result.Append(input[i]);
            }

            return result.ToString().Trim();
        }
    }
#endif
 
    public class Button2Attribute : PropertyAttribute
    {
        public string MethodName { get; }
        public Button2Attribute(string methodName)
        {
            MethodName = methodName;
        }
    }
}