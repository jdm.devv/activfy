﻿using UnityEngine;

namespace Activfy.Helpers
{
    public enum VectorDirections
    {
        /// <summary>
        /// (0,0,0)
        /// </summary>
        Zero,
        /// <summary>
        /// (0,0,1)
        /// </summary>
        Forward,
        /// <summary>
        /// (0,0,-1)
        /// </summary>
        Back,
        /// <summary>
        /// (0,1,0)
        /// </summary>
        Up,
        /// <summary>
        /// (0,-1,0)
        /// </summary>
        Down,
        /// <summary>
        /// (-1,0,0)
        /// </summary>
        Left,
        /// <summary>
        /// (1,0,0)
        /// </summary>
        Right
    }
}