﻿using System;
using System.Reflection;
using UnityEngine;

namespace Activfy.Helpers
{
    public static class ActivfiUtil
    {
        /// <summary>
        /// Safe way to add force without going over a max speed
        /// </summary>
        /// <param name="rigidbody"></param>
        /// <param name="acceleration"></param>
        /// <param name="maxMoveSpeed"></param>
        public static void AddForceSafe(ref Rigidbody rigidbody, Vector3 acceleration, float maxMoveSpeed)
        {
            Vector3 newVelocity = rigidbody.velocity;
            newVelocity += acceleration * (Time.fixedDeltaTime * rigidbody.mass);

            Vector3 direction = acceleration.normalized;
            Vector3 maxVelocity = direction * maxMoveSpeed;

            if (rigidbody.velocity.x > maxVelocity.x)
            {
                newVelocity.x = Mathf.Min(newVelocity.x, rigidbody.velocity.x);
            }
            else if (rigidbody.velocity.x < -maxVelocity.x)
            {
                newVelocity.x = Mathf.Max(newVelocity.x, rigidbody.velocity.x);
            }
            else
            {
                newVelocity.x = Mathf.Clamp(newVelocity.x, -maxVelocity.x, maxVelocity.x);
            }

            if (rigidbody.velocity.z > maxVelocity.z)
            {
                newVelocity.z = Mathf.Min(newVelocity.z, rigidbody.velocity.z);
            }
            else if (rigidbody.velocity.z < -maxVelocity.z)
            {
                newVelocity.z = Mathf.Max(newVelocity.z, rigidbody.velocity.z);
            }
            else
            {
                newVelocity.z = Mathf.Clamp(newVelocity.z, -maxVelocity.z, maxVelocity.z);
            }

            rigidbody.velocity = newVelocity;
        }

        public static Vector3Int VectorFromVectorDirections(VectorDirections direction)
        {
            return direction switch
            {
                VectorDirections.Forward => Vector3Int.forward,
                VectorDirections.Back => Vector3Int.back,
                VectorDirections.Up => Vector3Int.up,
                VectorDirections.Down => Vector3Int.down,
                VectorDirections.Left => Vector3Int.left,
                VectorDirections.Right => Vector3Int.right,
                VectorDirections.Zero => Vector3Int.zero,
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, null)
            };
        }

        public static VectorDirections VectorDirectionsFromVector(Vector3Int direction)
        {
            if (direction == Vector3Int.forward)
                return VectorDirections.Forward;
            if (direction == Vector3Int.back)
                return VectorDirections.Back;
            if (direction == Vector3Int.up)
                return VectorDirections.Up;
            if (direction == Vector3Int.down)
                return VectorDirections.Down;
            if (direction == Vector3Int.left)
                return VectorDirections.Left;
            if (direction == Vector3Int.right)
                return VectorDirections.Right;

            return VectorDirections.Zero;
        }

        public static void SetTargetRotationOfJoint(ConfigurableJoint joint,
            Quaternion targetRotation,
            Quaternion initialRotation,
            Rigidbody parentRigidbody,
            Quaternion parentInitialRotation)
        {
            if (joint.configuredInWorldSpace)
            {
                if (parentRigidbody)
                {
                    Quaternion upperParentRotDelta =
                        parentInitialRotation * Quaternion.Inverse(parentRigidbody.rotation);
                    Quaternion targetJointRot = upperParentRotDelta * targetRotation;

                    joint.SetTargetRotation(targetJointRot, initialRotation);
                }
                else
                {
                    if (targetRotation != initialRotation)
                        joint.SetTargetRotation(targetRotation, initialRotation);
                }
            }
            else
            {
                if (targetRotation != initialRotation)
                    joint.SetTargetRotationLocal(targetRotation, initialRotation);
            }
        }

        public static void SetTargetRotationOfJoint(ConfigurableJoint joint,
            Transform targetTransform,
            Quaternion initialRotation,
            Rigidbody parentRigidbody,
            Quaternion parentInitialRotation)
        {
            if (joint.configuredInWorldSpace)
            {
                Quaternion targetRotation = targetTransform.rotation;

                SetTargetRotationOfJoint(joint, targetRotation, initialRotation, parentRigidbody,
                    parentInitialRotation);
            }
            else
            {
                Quaternion targetRotation = targetTransform.localRotation;

                SetTargetRotationOfJoint(joint, targetRotation, initialRotation, parentRigidbody,
                    parentInitialRotation);
            }
        }

        public static void ApplyDrag(Rigidbody rigidBody, float dragCoefficient, float deltaTime)
        {
            rigidBody.velocity *= 1 - deltaTime * dragCoefficient;
        }

        public static void ApplyXZDrag(Rigidbody rigidBody, float dragCoefficient, float deltaTime)
        {
            Vector3 velocity = rigidBody.velocity;
            velocity.x *= 1 - deltaTime * dragCoefficient;
            velocity.z *= 1 - deltaTime * dragCoefficient;
            rigidBody.velocity = velocity;
        }

        public static void ApplyAngularDrag(Rigidbody rigidBody, float dragCoefficient, float deltaTime)
        {
            rigidBody.angularVelocity *= 1 - deltaTime * dragCoefficient;
        }

        public static Quaternion ShortestRotation(Quaternion a, Quaternion b)
        {
            if (Quaternion.Dot(a, b) < 0)
                return a * Quaternion.Inverse(Multiply(b, -1));
            return a * Quaternion.Inverse(b);
        }

        public static Quaternion Multiply(Quaternion input, float scalar)
        {
            return new Quaternion(input.x * scalar, input.y * scalar, input.z * scalar, input.w * scalar);
        }

        /// <summary>
        /// Copies all fields and properties from one component to another, including private/internal fields.
        /// </summary>
        /// <typeparam name="T">The type of the component to copy.</typeparam>
        /// <param name="original">The original component to copy from.</param>
        /// <param name="destination">The GameObject to copy the component to.</param>
        /// <returns>The newly created component on the target GameObject.</returns>
        public static T CopyComponent<T>(T original, GameObject destination) where T : Component
        {
            if (original == null || destination == null)
            {
                Debug.LogError(
                    "Original component or destination GameObject is null. Please provide valid references.");
                return null;
            }

            // Get the type of the original component
            System.Type type = original.GetType();

            // Add a new component of the same type to the destination GameObject
            T copiedComponent = destination.AddComponent(type) as T;

            if (copiedComponent == null)
            {
                Debug.LogError("Failed to create component of type " + type);
                return null;
            }

            // Copy all fields from the original component to the new one
            foreach (FieldInfo field in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic |
                                                       BindingFlags.Instance))
            {
                if (field.IsStatic) continue; // Skip static fields

                try
                {
                    object value = field.GetValue(original);
                    field.SetValue(copiedComponent, value);
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning($"Skipping field '{field.Name}' due to an exception: {e.Message}");
                    // Skip or handle the problematic field as needed
                }
            }

            // Copy all properties from the original component to the new one
            foreach (PropertyInfo prop in type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic |
                                                             BindingFlags.Instance))
            {
                if (!prop.CanWrite || prop.GetSetMethod(true) == null)
                    continue; // Skip if we can't write to the property

                try
                {
                    object value = prop.GetValue(original);
                    prop.SetValue(copiedComponent, value);
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning($"Skipping property '{prop.Name}' due to an exception: {e.Message}");
                    // Skip or handle the problematic property as needed
                }
            }

            return copiedComponent;
        }
    }
}